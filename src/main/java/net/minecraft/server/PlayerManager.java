package net.minecraft.server;

import java.util.Random;

public class PlayerManager {

    Random random = new Random();
    static Random staticRandom = new Random();
    Random seededRandom = new Random(1337);

    public Random getSeededRandom() {
        return this.seededRandom;
    }

    public void setRandom(Random random) {
        this.random = random;
    }

    public static void setStaticRand(Random random) {
        staticRandom = random;
    }

    public void sendPacket() {
    }

    public void onPlayerConnect() {
        System.nanoTime();
        sendPacket();
        while(true) {
            if (Boolean.TRUE) break;
            System.out.println(3);
        }
        sendPacket();
        return;
    }
    
    public int randomTest(int i) {
        Random rand = new Random();
        if (rand.nextBoolean()) {
            i = rand.nextInt();
            rand.nextInt(i-2);
        }
        while(rand.nextDouble() > 0.3174D) {
            if (rand.nextFloat() < 0.001) break;
        }
        sendPacket();
        return random.nextInt();
    }

    public final void protectedNonFinalMethod() {

    }
    public void packgePrivatefinalMethod() {

    }
    @SuppressWarnings({"unused"})
    private static final Object publicStaticField = null;
}
