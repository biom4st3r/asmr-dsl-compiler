package net.minecraft.server;

class AccessTest {
    public static final Object[] protectedStaticField = null;
    Void publicFinalSyntheticField;
    public final Void cleanField = null;
    protected final Void publicField = null;

    public final strictfp synchronized void cleanMethod() {}
    protected final void publicSyncMethod() {}
    public static final Object[] protectedStaticMethod() {return null;}
    Void publicFinalSyntheticMethod() {return null;}
}
