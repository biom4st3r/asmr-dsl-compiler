package biom4st3r.asmr_compiler.builder;

import biom4st3r.asmr_compiler.Biom4st3rCompilerError;
import org.quiltmc.asmr.processor.AsmrProcessor;
import org.quiltmc.asmr.processor.AsmrTransformer;
import org.quiltmc.asmr.processor.capture.AsmrSliceCapture;
import org.quiltmc.asmr.tree.AsmrValueListNode;
import org.quiltmc.asmr.tree.AsmrValueNode;

public final class Builder {

    public enum Pos {
        BEFORE,
        AFTER,
        ;
    }

    final AsmrProcessor processor;
    final AsmrTransformer transformer;
    private Builder(AsmrProcessor processor, AsmrTransformer transformer) {
        this.processor = processor;
        this.transformer = transformer;
    }

    // private static Map<String,BuilderClass> classMap = new ConcurrentHashMap<>();

    public static BuilderClass withClass(AsmrProcessor processor, AsmrTransformer transformer, String clazz) {
        Builder builder = new Builder(processor,transformer);
        // return classMap.computeIfAbsent(clazz, name->new BuilderClass(builder, name));
        return new BuilderClass(builder, clazz);
    }

    public static BuilderClass withAllClasses(AsmrProcessor processor, AsmrTransformer transformer) {
        return withClass(processor, transformer, "");
    }

    static int after(int i) {
        return i * 2 + 2;
    }

    static int before(int i) {
        return i * 2 + 1;
    }

    static AsmrSliceCapture<AsmrValueNode<Integer>> getModifierAdditionSlice(AsmrProcessor processor, AsmrValueListNode<Integer> modifiers, int modifier) {
        int start = -1336;
        int end = -1336;
        if(modifiers.size() == 0 || modifier < modifiers.get(0).value()) {
            start = 0;
            end = 0;
        } else if(modifier > modifiers.get(modifiers.size()-1).value()) {
            start = Builder.after(modifiers.size()-1);
            end = Builder.after(modifiers.size()-1);
        } else {
            for(int index = 0; index < modifiers.size(); index++) {
                if(modifiers.get(index).value() < modifier) {
                    start = Builder.after(index);
                    end = start;
                    break;
                }
            }
        }
        AsmrSliceCapture<AsmrValueNode<Integer>> cap = processor.refCapture(modifiers, start, end);
        if(start == -1336 || end == -1336) throw new Biom4st3rCompilerError("Invalid");
        return cap;
    }
}
