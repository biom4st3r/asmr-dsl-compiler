package biom4st3r.asmr_compiler.builder;

import java.util.function.Consumer;
import java.util.function.Function;

import biom4st3r.asmr_compiler.Biom4st3rCompilerError;
import biom4st3r.asmr_compiler.compilers.AccessTransformerCompiler;
import biom4st3r.asmr_compiler.compilers.utils.MethodTarget;
import biom4st3r.asmr_compiler.compilers.utils.TypeExploder;
import biom4st3r.asmr_compiler.insns_v2.Insn;
import org.quiltmc.asmr.processor.AsmrProcessor;
import org.quiltmc.asmr.processor.capture.AsmrNodeCapture;
import org.quiltmc.asmr.processor.capture.AsmrSliceCapture;
import org.quiltmc.asmr.tree.AsmrValueListNode;
import org.quiltmc.asmr.tree.AsmrValueNode;
import org.quiltmc.asmr.tree.member.AsmrMethodListNode;
import org.quiltmc.asmr.tree.member.AsmrMethodNode;
import org.quiltmc.asmr.tree.method.AsmrMethodBodyNode;

public class BuilderMethod {

    final BuilderClass builder;
    final AsmrProcessor processor;
    private final MethodTarget target;

    BuilderMethod(BuilderClass builderClass, String owner, String name, String desc) {
        this.builder = builderClass;
        this.processor = this.builder.processor;
        this.target = new MethodTarget(owner,name,desc);
    }

    public BuilderInsn matchInsn(Insn... insns) {
        return new BuilderInsn(this, insns);
    }

    void forMatchingMethods(Consumer<AsmrMethodNode> node) {
        builder.withClassTarget(acn-> {
            acn.methods().stream().filter(target::matches).forEach(node);
        });
    }

    public BuilderMethod replaceDesc(String desc) {
        forMatchingMethods(amn-> {
            AsmrNodeCapture<AsmrValueNode<String>> cap = processor.refCapture(amn.desc());
            processor.addWrite(builder.builder.transformer, cap, ()-> {
                AsmrValueNode<String> node = new AsmrValueNode<>();
                node.init(desc);
                return node;
            });
        });
        return this;
    }

    // TODO avoid lambdas somehow
    public BuilderMethod mutateDesc0(Function<String,String> func) {
        forMatchingMethods(amn-> {
            AsmrNodeCapture<AsmrValueNode<String>> cap = processor.refCapture(amn.desc());
            processor.addWrite(builder.builder.transformer, cap, ()-> {
                AsmrValueNode<String> node = new AsmrValueNode<>();
                node.init(func.apply(amn.desc().value()));
                return node;
            });
        });
        return this;
    }

    public BuilderMethod replaceName(String name) {
        forMatchingMethods(amn-> {
            AsmrNodeCapture<AsmrValueNode<String>> cap = processor.refCapture(amn.name());
            processor.addWrite(builder.builder.transformer, cap, ()-> {
                AsmrValueNode<String> node = new AsmrValueNode<>();
                node.init(name);
                return node;
            });
        });
        return this;
    }

    public BuilderMethod removeModifier(int modify) {
        forMatchingMethods(amn-> {
            for(int mod = 0; mod < amn.modifiers().size(); mod++) {
                AsmrValueNode<Integer> modifier = amn.modifiers().get(mod);
                if(modify == modifier.value().intValue()) {
                    AsmrSliceCapture<AsmrValueNode<Integer>> cap = processor.refCapture(amn.modifiers(), Builder.before(mod), Builder.after(mod));

                    System.out.println(AccessTransformerCompiler.map.entrySet().stream().filter(e->e.getValue().intValue() == modify).findAny().get().getKey());
                    System.out.println(String.format("removing: %s %s %s", target.owner, target.name, target.desc));
                    System.out.println(String.format("start(%s) end(%s)", cap.startIndexInclusive(), cap.endIndexExclusive()));
                    processor.addWrite(builder.builder.transformer, cap, ()-> {
                        return new AsmrValueListNode<>();
                    });
                }
            }
        });
        return this;
    }

    public BuilderMethod addModifier(int modifier) {
        forMatchingMethods(amn-> {
            if(amn.modifiers().stream().mapToInt(AsmrValueNode::value).filter(val->val == modifier).findAny().isPresent()) {
                return;
            }
            AsmrSliceCapture<AsmrValueNode<Integer>> cap = Builder.getModifierAdditionSlice(processor, amn.modifiers(), modifier);
            System.out.println(AccessTransformerCompiler.map.entrySet().stream().filter(e->e.getValue().intValue() == modifier).findAny().get().getKey());
            System.out.println(String.format("Adding: %s %s %s", target.owner, target.name, target.desc));
            System.out.println(String.format("start(%s) end(%s)", cap.startIndexInclusive(), cap.endIndexExclusive()));
            processor.addWrite(builder.builder.transformer, cap, ()-> {
                AsmrValueListNode<Integer> list =  new AsmrValueListNode<Integer>();
                AsmrValueNode<Integer> vn = new AsmrValueNode<>();
                vn.init(modifier);
                list.addCopy(vn);
                return list;
            });
        });
        return this;
    }

    public BuilderClass replace(AsmrMethodNode node) {
        forMatchingMethods(amn-> {
            AsmrNodeCapture<AsmrMethodNode> cap = processor.refCapture(amn);
            processor.addWrite(builder.builder.transformer, cap, ()->{
                return node;
            });
        });
        return builder;
    }

    public BuilderMethod replaceBody(AsmrMethodBodyNode node) {
        forMatchingMethods(amn-> {
            AsmrNodeCapture<AsmrMethodBodyNode> cap = processor.refCapture(amn.body());
            processor.addWrite(builder.builder.transformer, cap, ()->{
                return node;
            });
        });
        return this;
    }

    public BuilderMethod inject(AsmrMethodNode node) {
        builder.withClassTarget(acn-> {
            int index = Builder.before(acn.methods().size()-1);
            AsmrSliceCapture<AsmrMethodNode> cap = processor.refCapture(acn.methods(), index, index);
            processor.addWrite(builder.builder.transformer, cap, ()-> {
                AsmrMethodListNode list = new AsmrMethodListNode();
                list.addCopy(node);
                return list;
            });
        });
        return this;
    }

    public BuilderClass done() {
        return builder;
    }

    @SuppressWarnings("unused")
    private void _crash_(String s) {
        throw new Biom4st3rCompilerError("%s\n%s", s,this.toString());
    }

    @Override
    public String toString() {
        return new StringBuilder()
            .append(builder.toString())
            .append("\n")
            .append("[BuilderMethod] ")
            .append(target.toString())
            .toString();
    }
    
}
