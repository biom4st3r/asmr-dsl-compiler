package biom4st3r.asmr_compiler.builder;

import java.util.function.Consumer;

import biom4st3r.asmr_compiler.Biom4st3rCompilerError;
import biom4st3r.asmr_compiler.compilers.utils.FieldTarget;
import org.quiltmc.asmr.processor.AsmrProcessor;
import org.quiltmc.asmr.processor.capture.AsmrNodeCapture;
import org.quiltmc.asmr.processor.capture.AsmrSliceCapture;
import org.quiltmc.asmr.tree.AsmrValueListNode;
import org.quiltmc.asmr.tree.AsmrValueNode;
import org.quiltmc.asmr.tree.member.AsmrFieldListNode;
import org.quiltmc.asmr.tree.member.AsmrFieldNode;

public class BuilderField {
    final BuilderClass builder;
    final AsmrProcessor processor;
    private final FieldTarget target;

    BuilderField(BuilderClass builderClass, String owner, String name, String desc) {
        this.builder = builderClass;
        this.processor = this.builder.processor;
        this.target = new FieldTarget(owner,name,desc);
    }

    private void forMatchingFields(Consumer<AsmrFieldNode> consumer) {
        builder.withClassTarget(acn-> {
            acn.fields().stream().filter(target::matches).forEach(consumer);
        });
    }

    @SuppressWarnings("unused")
    private void _crash_(String s) {
        throw new Biom4st3rCompilerError("%s\n%s", s,this.toString());
    }

    public BuilderField replaceDesc(String desc) {
        forMatchingFields(afn->{
            AsmrNodeCapture<AsmrValueNode<String>> cap = processor.refCapture(afn.desc());
            processor.addWrite(builder.builder.transformer, cap, ()-> {
                AsmrValueNode<String> node = new AsmrValueNode<>();
                node.init(desc);
                return node;
            });
        });
        return this;
    }

    public BuilderField replaceName(String name) {
        forMatchingFields(afn-> {
            AsmrNodeCapture<AsmrValueNode<String>> cap = processor.refCapture(afn.name());
            processor.addWrite(builder.builder.transformer, cap, ()-> {
                AsmrValueNode<String> node = new AsmrValueNode<>();
                node.init(name);
                return node;
            });
        });
        return this;
    }

    public BuilderField removeModifier(int modify) {
        forMatchingFields(afn-> {
            for(int mod = 0; mod < afn.modifiers().size(); mod++) {
                AsmrValueNode<Integer> modifier = afn.modifiers().get(mod);
                if(modify == modifier.value().intValue()) {
                    AsmrSliceCapture<AsmrValueNode<Integer>> cap = processor.refCapture(afn.modifiers(), Builder.before(mod), Builder.after(mod));
                    System.out.println(String.format("removing: %s %s %s", target.owner, target.name, target.desc));
                    System.out.println(String.format("start(%s) end(%s)", cap.startIndexInclusive(), cap.endIndexExclusive()));
                    processor.addWrite(builder.builder.transformer, cap, ()-> {
                        return new AsmrValueListNode<>();
                    });
                }
            }
        });
        return this;
    }

    public BuilderField addModifier(int modifier) {
        forMatchingFields(afn-> {
            if(afn.modifiers().stream().mapToInt(AsmrValueNode::value).filter(val->val == modifier).findAny().isPresent()) {
                return;
            }
            AsmrSliceCapture<AsmrValueNode<Integer>> cap = Builder.getModifierAdditionSlice(processor, afn.modifiers(), modifier);
            System.out.println(String.format("Adding: %s %s %s", target.owner, target.name, target.desc));
            System.out.println(String.format("start(%s) end(%s)", cap.startIndexInclusive(), cap.endIndexExclusive()));
            processor.addWrite(builder.builder.transformer, cap, ()-> {
                AsmrValueListNode<Integer> list =  new AsmrValueListNode<Integer>();
                AsmrValueNode<Integer> vn = new AsmrValueNode<>();
                vn.init(modifier);
                list.addCopy(vn);
                return list;
            });
        });
        return this;
    }

    public BuilderField replace(AsmrFieldNode node) {
        forMatchingFields(afn->{
            AsmrNodeCapture<AsmrFieldNode> cap = processor.refCapture(afn);
            processor.addWrite(builder.builder.transformer, cap, ()-> {
                return node;
            });
        });
        return this;
    }

    public BuilderField inject(AsmrFieldNode node) {
        builder.withClassTarget(acn-> {
            int index = Builder.before(acn.fields().size()-1);
            AsmrSliceCapture<AsmrFieldNode> cap = processor.refCapture(acn.fields(), index, index);
            processor.addWrite(builder.builder.transformer, cap, ()-> {
                AsmrFieldListNode list = new AsmrFieldListNode();
                list.addCopy(node);
                return list;
            });
            return;
        });
        return this;
    }

    public BuilderClass done() {
        return builder;
    }
 
    @Override
    public String toString() {
        return new StringBuilder().append(builder.toString()).append("\n")
            .append("[BuilderField] ").append(target.toString()).toString();
    }
}
