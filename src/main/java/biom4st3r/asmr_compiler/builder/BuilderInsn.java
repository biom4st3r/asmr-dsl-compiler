package biom4st3r.asmr_compiler.builder;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Function;

import biom4st3r.asmr_compiler.Biom4st3rCompilerError;
import biom4st3r.asmr_compiler.builder.Builder.Pos;
import biom4st3r.asmr_compiler.compilers.utils.InsnsTarget;
import biom4st3r.asmr_compiler.insns_v2.FieldInsn;
import biom4st3r.asmr_compiler.insns_v2.Insn;
import biom4st3r.asmr_compiler.insns_v2.IntInsn;
import biom4st3r.asmr_compiler.insns_v2.MethodInsn;
import biom4st3r.asmr_compiler.insns_v2.TypeInsn;
import biom4st3r.asmr_compiler.insns_v2.VarInsn;
import org.quiltmc.asmr.processor.AsmrProcessor;
import org.quiltmc.asmr.processor.capture.AsmrNodeCapture;
import org.quiltmc.asmr.processor.capture.AsmrSliceCapture;
import org.quiltmc.asmr.tree.AsmrValueNode;
import org.quiltmc.asmr.tree.insn.AsmrFieldInsnNode;
import org.quiltmc.asmr.tree.insn.AsmrInstructionListNode;
import org.quiltmc.asmr.tree.insn.AsmrIntInsnNode;
import org.quiltmc.asmr.tree.insn.AsmrLdcInsnNode;
import org.quiltmc.asmr.tree.insn.AsmrMethodInsnNode;
import org.quiltmc.asmr.tree.insn.AsmrTypeInsnNode;
import org.quiltmc.asmr.tree.insn.AsmrVarInsnNode;
import org.quiltmc.asmr.tree.method.AsmrIndex;
import org.quiltmc.asmr.tree.method.AsmrMethodBodyNode;

public class BuilderInsn {

    final BuilderMethod builder;
    final AsmrProcessor processor;
    private final InsnsTarget target;

    BuilderInsn(BuilderMethod builderMethod, Insn... insns) {
        this.builder = builderMethod;
        this.processor = this.builder.processor;
        target = new InsnsTarget(insns);
    }
    private void forMatchingInsns(BiConsumer<AsmrInstructionListNode<?>,Integer> consumer) {
        builder.forMatchingMethods(amn-> {
            int matchIndex = 0;
            for(int i = 0; i < amn.body().instructions().size(); i++) {
                if(matchIndex > 0) {
                    if(target.opcodes.length == matchIndex) {
                        // FULLY MATCHED
                        consumer.accept(amn.body().instructions(), i);
                    }
                }
                if(target.opcodes[matchIndex].matches(amn.body().instructions().get(i))) {
                    matchIndex++; // 1 before it already matched 0
                } else matchIndex = 0;
            }
        });
    }
    
    private void forMatchingInsns(BiConsumer<AsmrInstructionListNode<?>,Integer> consumer, int ordinal) {
        builder.forMatchingMethods(amn-> {
            int o = -1;
            int matchIndex = 0;
            for(int i = 0; i < amn.body().instructions().size(); i++) {
                if(matchIndex > 0) {
                    if(target.opcodes.length == matchIndex) {
                        o++;
                        if(ordinal == o) {
                            // FULLY MATCHED
                            consumer.accept(amn.body().instructions(), i);
                            break;
                        }
                    }
                }
                if(target.opcodes[matchIndex].matches(amn.body().instructions().get(i))) {
                    matchIndex++; // 1 before it already matched 0
                } else matchIndex = 0;
            }
        });
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public BuilderInsn replace(AsmrInstructionListNode<?> node) {
        forMatchingInsns((insns,endIndex)-> {
            AsmrSliceCapture cap = processor.refCapture(insns, Builder.before(endIndex-target.opcodes.length), Builder.after(endIndex));
            processor.addWrite(builder.builder.builder.transformer, cap, ()-> {
                return node;
            });
        });
        return this;
    }
    @SuppressWarnings({"unchecked", "rawtypes"})
    public BuilderInsn replace(int ordinal, AsmrInstructionListNode<?> node) {
        forMatchingInsns((insns,endIndex)-> {
            AsmrSliceCapture cap = processor.refCapture(insns, Builder.before(endIndex-target.opcodes.length), Builder.after(endIndex));
            processor.addWrite(builder.builder.builder.transformer, cap, ()-> {
                return node;
            });
        }, ordinal);
        return this;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public BuilderInsn injectBefore(List<Insn> node) {
        forMatchingInsns((insns,endIndex)-> {
            AsmrSliceCapture cap = processor.refCapture(insns, Builder.before(endIndex-target.opcodes.length), Builder.before(endIndex-target.opcodes.length));
            processor.addWrite(builder.builder.builder.transformer, cap, ()-> {
                return Insn.toAsmrInsnListNode(node, ((AsmrMethodBodyNode)insns.parent()));
            });
        });
        return this;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public BuilderInsn injectBefore(int ordinal, List<Insn> node) {
        if(ordinal == -1) return injectBefore(node);
        forMatchingInsns((insns,endIndex)-> {
            AsmrSliceCapture cap = processor.refCapture(insns, Builder.before(endIndex-target.opcodes.length), Builder.before(endIndex-target.opcodes.length));
            processor.addWrite(builder.builder.builder.transformer, cap, ()-> {
                return Insn.toAsmrInsnListNode(node, ((AsmrMethodBodyNode)insns.parent()));
            });
        },ordinal);
        return this;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public BuilderInsn injectAfter(List<Insn> node) {
        forMatchingInsns((insns,endIndex)-> {
            AsmrSliceCapture cap = processor.refCapture(insns, Builder.after(endIndex), Builder.after(endIndex));
            processor.addWrite(builder.builder.builder.transformer, cap, ()-> {
                return Insn.toAsmrInsnListNode(node, ((AsmrMethodBodyNode)insns.parent()));
            });
        });
        return this;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public BuilderInsn injectAfter(int ordinal, List<Insn> node) {
        if(ordinal == -1) return injectAfter(node);
        forMatchingInsns((insns,endIndex)-> {
            AsmrSliceCapture cap = processor.refCapture(insns, Builder.after(endIndex), Builder.after(endIndex));
            processor.addWrite(builder.builder.builder.transformer, cap, ()-> {
                return Insn.toAsmrInsnListNode(node, ((AsmrMethodBodyNode)insns.parent()));
            });
        },ordinal);
        return this;
    }

    public BuilderInsn inject(Pos pos, List<Insn> node) {
        return pos == Pos.BEFORE ? injectBefore(node) : injectAfter(node);
    }

    public BuilderInsn inject(int ordinal, Pos pos, List<Insn> node) {
        return pos == Pos.BEFORE ? injectBefore(ordinal, node) : injectAfter(ordinal, node);
    }

    public BuilderInsn inject(List<Insn> node) {
        return injectBefore(node);
    }

    @SuppressWarnings({"unchecked"})
    private BuilderInsn _replaceConstant(Object o) {
        if(this.target.opcodes.length > 1) _crash_("must only target 1 insn");
        forMatchingInsns((insns,endIndex)-> {
            AsmrNodeCapture<AsmrValueNode<Object>> cap =  (AsmrNodeCapture<AsmrValueNode<Object>>) processor.refCapture(((AsmrLdcInsnNode)insns.get(endIndex)).cstList().get(0));
            processor.addWrite(builder.builder.builder.transformer, cap, ()-> {
                return new AsmrValueNode<Object>().init(o);
            });
        });
        return this;
    }

    public BuilderInsn replaceConstant(Integer o) {
        return _replaceConstant(o);
    }
    public BuilderInsn replaceConstant(Long o) {
        return _replaceConstant(o);
    }
    public BuilderInsn replaceConstant(String o) {
        return _replaceConstant(o);
    }
    public BuilderInsn replaceConstant(Float o) {
        return _replaceConstant(o);
    }
    public BuilderInsn replaceConstant(Double o) {
        return _replaceConstant(o);
    }

    private void replaceFieldMethodStuff(String s, Function<AsmrMethodInsnNode,AsmrValueNode<String>> method, Function<AsmrFieldInsnNode,AsmrValueNode<String>> field) {
        if(this.target.opcodes.length > 1) _crash_("must only target 1 insn");
        final char type;
        if(this.target.opcodes[0] instanceof MethodInsn) {
            type = 'm';
        } else if(this.target.opcodes[0] instanceof FieldInsn) {
            type = 'f';
        } else {
            _crash_("targeted insn wasn't a method or field insn");
            type = ' '; // This never gets called
        }
        forMatchingInsns((insns,endIndex)-> {
            AsmrNodeCapture<AsmrValueNode<String>> cap;
            if(type == 'm') {
                cap = processor.refCapture(method.apply((AsmrMethodInsnNode) insns.get(endIndex)));
            } else {
                cap = processor.refCapture(field.apply((AsmrFieldInsnNode) insns.get(endIndex)));
            }
            processor.addWrite(builder.builder.builder.transformer, cap, ()-> {
                return new AsmrValueNode<String>().init(s);
            });
        });
    }

    public BuilderInsn replaceOwner(String newOwner) {
        this.replaceFieldMethodStuff(newOwner, AsmrMethodInsnNode::owner, AsmrFieldInsnNode::owner);
        return this;
    }
    public BuilderInsn replaceName(String newName) {
        this.replaceFieldMethodStuff(newName, AsmrMethodInsnNode::name, AsmrFieldInsnNode::name);
        return this;
    }
    public BuilderInsn replaceDesc(String newDesc) {
        this.replaceFieldMethodStuff(newDesc, AsmrMethodInsnNode::desc, AsmrFieldInsnNode::desc);
        return this;
    }

    public BuilderInsn replaceIntOperand(int newIntOperand) {
        if(this.target.opcodes.length > 1) _crash_("must only target 1 insn");
        if(!(this.target.opcodes[0] instanceof IntInsn)) _crash_("Tartet insn must be IntOperandInsn");
        forMatchingInsns((insns,endIndex)-> {
            AsmrIntInsnNode intnode = (AsmrIntInsnNode) insns.get(endIndex);
            AsmrNodeCapture<AsmrValueNode<Integer>> cap = processor.refCapture(intnode.operand());
            processor.addWrite(builder.builder.builder.transformer, cap, ()-> {
                return new AsmrValueNode<Integer>().init(newIntOperand);
            });
        });
        return this;
    }

    public BuilderInsn replaceType(String newType) {
        if(this.target.opcodes.length > 1) _crash_("must only target 1 insn");
        if(!(this.target.opcodes[0] instanceof TypeInsn)) _crash_("Tartet insn must be TypeInsn");
        forMatchingInsns((insns,endIndex)-> {
            AsmrTypeInsnNode intnode = (AsmrTypeInsnNode) insns.get(endIndex);
            AsmrNodeCapture<AsmrValueNode<String>> cap = processor.refCapture(intnode.desc());
            processor.addWrite(builder.builder.builder.transformer, cap, ()-> {
                return new AsmrValueNode<String>().init(newType);
            });
        });
        return this;
    }
    public BuilderInsn replaceVar(int newVarIndex) {
        if(this.target.opcodes.length > 1) _crash_("must only target 1 insn");
        if(!(this.target.opcodes[0] instanceof VarInsn)) _crash_("Tartet insn must be VarInsn");
        forMatchingInsns((insns,endIndex)-> {
            AsmrVarInsnNode intnode = (AsmrVarInsnNode) insns.get(endIndex);
            AsmrNodeCapture<AsmrValueNode<AsmrIndex>> cap = processor.refCapture(intnode.varIndex());
            processor.addWrite(builder.builder.builder.transformer, cap, ()-> {
                AsmrMethodBodyNode body = (AsmrMethodBodyNode) insns.parent();
                return new AsmrValueNode<AsmrIndex>().init(body.localVariables().get(newVarIndex).end().value());
            });
        });
        return this;
    }

    public BuilderMethod done() {
        return builder;
    }

    private void _crash_(String s) {
        throw new Biom4st3rCompilerError("%s\n%s", s,this.toString());
    }

    @Override
    public String toString() {
        return new StringBuilder()
            .append(builder.toString())
            .append("\n")
            .append("[BuilderInsn] ")
            .append(target.toString())
            .toString();
    }
}
