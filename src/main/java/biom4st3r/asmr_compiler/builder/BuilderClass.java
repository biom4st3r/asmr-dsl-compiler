package biom4st3r.asmr_compiler.builder;

import java.util.function.Consumer;

import biom4st3r.asmr_compiler.Biom4st3rCompilerError;
import biom4st3r.asmr_compiler.compilers.AccessTransformerCompiler;
import org.quiltmc.asmr.processor.AsmrProcessor;
import org.quiltmc.asmr.processor.capture.AsmrNodeCapture;
import org.quiltmc.asmr.processor.capture.AsmrSliceCapture;
import org.quiltmc.asmr.tree.AsmrValueListNode;
import org.quiltmc.asmr.tree.AsmrValueNode;
import org.quiltmc.asmr.tree.member.AsmrClassNode;

public class BuilderClass {

    final Builder builder;
    final AsmrProcessor processor;
    private final String targetClazz;
    
    BuilderClass(Builder builder, String clazz) {
        this.builder = builder;
        this.processor = builder.processor;
        this.targetClazz = clazz;
    }

    public void withClassTarget(Consumer<AsmrClassNode> node) {
        // TODO Crash on no classes matched
        if(targetClazz.isEmpty()) {
            processor.withAllClasses(node);
        } else {
            processor.withClass(targetClazz, node);
        }
    }

    public BuilderMethod withMethod(String owner, String name, String desc) {
        return new BuilderMethod(this, owner, name, desc);
    }

    public BuilderField withField(String owner, String name, String desc) {
        return new BuilderField(this, owner, name, desc);
    }

    public BuilderMethod withAllMethods() {
        return this.withMethod("#SKIP", "#SKIP", "#SKIP");
    }

    public Builder replace(AsmrClassNode node) {
        withClassTarget(acn->{
            AsmrNodeCapture<AsmrClassNode> i = processor.refCapture(acn);
            processor.addWrite(builder.transformer, i, ()->node);
        });
        return builder;
    }

    public BuilderClass removeModifier(int modify) {
        withClassTarget(acn-> {
            for(int modIndex = 0; modIndex < acn.modifiers().size(); modIndex++) {
                AsmrValueNode<Integer> mod = acn.modifiers().get(modIndex);
                if(modify == mod.value()) {
                    System.out.println(AccessTransformerCompiler.map.entrySet().stream().filter(e->e.getValue().intValue() == modify).findAny().get().getKey());
                    System.out.println(String.format("removing: %s", targetClazz));
                    
                    AsmrSliceCapture<AsmrValueNode<Integer>> cap = processor.refCapture(acn.modifiers(), Builder.before(modIndex), Builder.after(modIndex));
                    System.out.println(String.format("start(%s) end(%s)", cap.startIndexInclusive(), cap.endIndexExclusive()));
                    processor.addWrite(builder.transformer, cap, ()-> {
                        return new AsmrValueListNode<Integer>();
                    });
                }
            }
        });
        return this;
    }

    public BuilderClass addModifier(int modifier) {
        withClassTarget(acn -> {
            if(acn.modifiers().stream().mapToInt(val->val.value()).filter(val->val == modifier).findAny().isPresent()) {
                return;
            }
            AsmrSliceCapture<AsmrValueNode<Integer>> cap = Builder.getModifierAdditionSlice(processor, acn.modifiers(), modifier);

            System.out.println(AccessTransformerCompiler.map.entrySet().stream().filter(e->e.getValue().intValue() == modifier).findAny().get().getKey());
            System.out.println(String.format("Adding: %s", targetClazz));
            System.out.println(String.format("start(%s) end(%s)", cap.startIndexInclusive(), cap.endIndexExclusive()));
            processor.addWrite(builder.transformer, cap, ()-> {
                AsmrValueListNode<Integer> list =  new AsmrValueListNode<Integer>();
                AsmrValueNode<Integer> vn = new AsmrValueNode<>();
                vn.init(modifier);
                list.addCopy(vn);
                return list;
            });
        });
        return this;
    }

    public Builder done() {
        return builder;
    }

    @SuppressWarnings("unused")
    private void _crash_(String s) {
        throw new Biom4st3rCompilerError("%s\n%s", s,this.toString());
    }

    @Override
    public String toString() {
        return new StringBuilder().append("[BuilderClass] Target class: ").append(targetClazz.isEmpty() ? "ANY" : targetClazz).toString();
    }
    
}
