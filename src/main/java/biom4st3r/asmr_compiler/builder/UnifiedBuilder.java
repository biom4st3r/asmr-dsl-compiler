package biom4st3r.asmr_compiler.builder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

import biom4st3r.asmr_compiler.compilers.utils.Pair;
import org.quiltmc.asmr.processor.AsmrProcessor;
import org.quiltmc.asmr.processor.AsmrTransformer;
import org.quiltmc.asmr.processor.capture.AsmrNodeCapture;
import org.quiltmc.asmr.processor.capture.AsmrSliceCapture;
import org.quiltmc.asmr.tree.AsmrAbstractListNode;
import org.quiltmc.asmr.tree.AsmrNode;
import org.quiltmc.asmr.tree.AsmrValueListNode;
import org.quiltmc.asmr.tree.AsmrValueNode;
import org.quiltmc.asmr.tree.insn.AsmrAbstractInsnNode;
import org.quiltmc.asmr.tree.insn.AsmrFieldInsnNode;
import org.quiltmc.asmr.tree.insn.AsmrInstructionListNode;
import org.quiltmc.asmr.tree.insn.AsmrIntInsnNode;
import org.quiltmc.asmr.tree.insn.AsmrJumpInsnNode;
import org.quiltmc.asmr.tree.insn.AsmrMethodInsnNode;
import org.quiltmc.asmr.tree.insn.AsmrNoOperandInsnNode;
import org.quiltmc.asmr.tree.insn.AsmrTypeInsnNode;
import org.quiltmc.asmr.tree.insn.AsmrVarInsnNode;
import org.quiltmc.asmr.tree.member.AsmrClassNode;
import org.quiltmc.asmr.tree.member.AsmrFieldListNode;
import org.quiltmc.asmr.tree.member.AsmrFieldNode;
import org.quiltmc.asmr.tree.member.AsmrMethodListNode;
import org.quiltmc.asmr.tree.member.AsmrMethodNode;
import org.quiltmc.asmr.tree.method.AsmrIndex;
import org.quiltmc.asmr.tree.method.AsmrLocalVariableNode;
import org.quiltmc.asmr.tree.method.AsmrMethodBodyNode;

public class UnifiedBuilder {

    public static final String skip = "#SKIP";
    //#region Builder
    AsmrProcessor processor;
    AsmrTransformer transformer;

    public UnifiedBuilder withClass(AsmrProcessor processor, AsmrTransformer transformer, String clazz) {
        this.processor = processor;
        this.transformer = transformer;
        this.initBuilderClass(clazz);
        return this;
    }

    public UnifiedBuilder withAllClasses(AsmrProcessor processor, AsmrTransformer transformer) {
        this.processor = processor;
        this.transformer = transformer;
        this.initBuilderClass("");
        return this;
    }

    private static int after(int i) {
        return i * 2 + 2;
    }

    private static int before(int i) {
        return i * 2 + 1;
    }

    private static AsmrSliceCapture<AsmrValueNode<Integer>> getModifierAdditionSlice(AsmrProcessor processor, AsmrValueListNode<Integer> modifiers, int modifier) {
        int start = -1336;
        int end = -1336;
        if(modifiers.size() == 0 || modifier < modifiers.get(0).value()) {
            start = before(0);
            end = before(0);
        } else if(modifier > modifiers.get(modifiers.size()-1).value()) {
            start = after(modifiers.size()-1);
            end = after(modifiers.size()-1);
        } else {
            for(int index = 0; index < modifiers.size(); index++) {
                if(modifiers.get(index).value() < modifier) {
                    start = after(index);
                    end = start;
                    break;
                }
            }
        }
        AsmrSliceCapture<AsmrValueNode<Integer>> cap = processor.refCapture(modifiers, start, end);
        if(start == -1336 || end == -1336) throw new RuntimeException("Invalid");
        return cap;
    }
    //#endregion
    
    //#region Class
    String targetClazz;
    
    public UnifiedBuilder initBuilderClass(String clazz) {
        this.targetClazz = clazz;
        return this;
    }

    private void withClassTarget(Consumer<AsmrClassNode> node) {
        if(targetClazz.isEmpty()) {
            processor.withAllClasses(node);
        } else {
            processor.withClass(targetClazz, node);
        }
    }

    public UnifiedBuilder withMethod(String owner, String name, String desc) {
        return this.initBuilderMethod(owner, name, desc);
    }

    public UnifiedBuilder withField(String owner, String name, String desc) {
        return initBuilderField(owner, name, desc);
    }

    public UnifiedBuilder withAllMethods() {
        return this.initBuilderMethod("#SKIP", "#SKIP", "#SKIP");
    }

    public UnifiedBuilder replace(AsmrClassNode node) {
        withClassTarget(acn-> {
            AsmrNodeCapture<AsmrClassNode> i = processor.refCapture(acn);
            processor.addWrite(transformer, i, ()->node);
        });
        return this;
    }

    public UnifiedBuilder class_removeModifier(int modify) {
        String target = this.targetClazz;
        withClassTarget(acn-> {
            for(int modIndex = 0; modIndex < acn.modifiers().size(); modIndex++) {
                AsmrValueNode<Integer> mod = acn.modifiers().get(modIndex);
                if(modify == mod.value()) {
                    System.out.println(String.format("removing: %s", target));
                    
                    AsmrSliceCapture<AsmrValueNode<Integer>> cap = processor.refCapture(acn.modifiers(), before(modIndex), after(modIndex));
                    System.out.println(String.format("start(%s) end(%s)", cap.startIndexInclusive(), cap.endIndexExclusive()));
                    processor.addWrite(transformer, cap, ()-> {
                        return new AsmrValueListNode<Integer>();
                    });
                }
            }
        });
        return this;
    }

    public UnifiedBuilder class_addModifier(int modifier) {
        String target = this.targetClazz;
        withClassTarget(acn -> {
            if(acn.modifiers().stream().mapToInt(val->val.value()).filter(val->val == modifier).findAny().isPresent()) {
                return;
            }
            AsmrSliceCapture<AsmrValueNode<Integer>> cap = getModifierAdditionSlice(processor, acn.modifiers(), modifier);

            System.out.println(String.format("Adding: %s", target));
            System.out.println(String.format("start(%s) end(%s)", cap.startIndexInclusive(), cap.endIndexExclusive()));
            processor.addWrite(transformer, cap, ()-> {
                AsmrValueListNode<Integer> list =  new AsmrValueListNode<Integer>();
                AsmrValueNode<Integer> vn = new AsmrValueNode<>();
                vn.init(modifier);
                list.addCopy(vn);
                return list;
            });
        });
        return this;
    }

    private void un_initClass() {
        this.targetClazz = null;
    }

    public UnifiedBuilder doneWithClass() {
        un_initClass();
        un_initField();
        un_initMethod();
        return this;
    }
    //#endregion

    //#region Method
    String methodOwner;
    String methodName;
    String methodDesc;

    public UnifiedBuilder initBuilderMethod(String owner, String name, String desc) {
        this.methodName = name;
        this.methodOwner = owner;
        this.methodDesc = desc;
        return this;
    }

    public UnifiedBuilder matchInsn(Object[]... insns) {
        return initBuilderInsn(insns);
    }

    public UnifiedBuilder method_transformDesc(String desc) {
        this.withClassTarget(acn-> {
            for(int i = 0; i < acn.methods().size(); i++) {
                AsmrMethodNode amn = acn.methods().get(i);
                AsmrNodeCapture<AsmrValueNode<String>> cap = processor.refCapture(amn.desc());
                processor.addWrite(transformer, cap, ()-> {
                    AsmrValueNode<String> node = new AsmrValueNode<>();
                    node.init(desc);
                    return node;
                });
            }
        });
        return this;
    }

    public UnifiedBuilder method_transformDesc() {
        this.withClassTarget(acn-> {
            for(int i = 0; i < acn.methods().size(); i++) {
                AsmrMethodNode amn = acn.methods().get(i);
                AsmrNodeCapture<AsmrValueNode<String>> cap = processor.refCapture(amn.desc());
                processor.addWrite(transformer, cap, ()-> {
                    AsmrValueNode<String> node = new AsmrValueNode<>();
                    node.init(method_avoidInvokeDynamicTransformDesc(amn.desc().value()));
                    return node;
                });
            }
        });
        return this;
    }

    public String method_avoidInvokeDynamicTransformDesc(String s) {
        return null;
    }

    public UnifiedBuilder method_transformName(String name) {
        this.withClassTarget(acn-> {
            for(int i = 0; i < acn.methods().size(); i++) {
                AsmrMethodNode amn = acn.methods().get(i);
                AsmrNodeCapture<AsmrValueNode<String>> cap = processor.refCapture(amn.name());
                processor.addWrite(transformer, cap, ()-> {
                    AsmrValueNode<String> node = new AsmrValueNode<>();
                    node.init(name);
                    return node;
                });
            }
        });
        return this;
    }

    public UnifiedBuilder method_removeModifier(int modify) {
        String owner = this.methodOwner;
        String name = this.methodName;
        String desc = this.methodDesc;
        this.withClassTarget(acn-> {
            for(int method = 0; method < acn.methods().size(); method++) {
                AsmrMethodNode amn = acn.methods().get(method);
                if(!matchesMethod(amn,owner,name,desc)) continue;
                for(int mod = 0; mod < amn.modifiers().size(); mod++) {
                    AsmrValueNode<Integer> modifier = amn.modifiers().get(mod);
                    if(modify == modifier.value().intValue()) {
                        AsmrSliceCapture<AsmrValueNode<Integer>> cap = processor.refCapture(amn.modifiers(), before(mod), after(mod));

                        System.out.println(String.format("removing: %s %s %s", owner, name, desc));
                        System.out.println(String.format("start(%s) end(%s)", cap.startIndexInclusive(), cap.endIndexExclusive()));
                        processor.addWrite(transformer, cap, ()-> {
                            return new AsmrValueListNode<>();
                        });
                    }
                }
            }
        });
        return this;
    }

    public UnifiedBuilder method_addModifier(int modifier) {
        String owner = this.methodOwner;
        String name = this.methodName;
        String desc = this.methodDesc;
        this.withClassTarget(acn-> {
            for(int method = 0; method < acn.methods().size(); method++) {
                AsmrMethodNode amn = acn.methods().get(method);
                if(!matchesMethod(amn,owner,name,desc)) continue;
                if(amn.modifiers().stream().mapToInt(AsmrValueNode::value).filter(val->val == modifier).findAny().isPresent()) {
                    return;
                }
                AsmrSliceCapture<AsmrValueNode<Integer>> cap = getModifierAdditionSlice(processor, amn.modifiers(), modifier);

                System.out.println(String.format("Adding: %s %s %s", this.methodOwner, this.methodName, this.methodDesc));
                System.out.println(String.format("start(%s) end(%s)", cap.startIndexInclusive(), cap.endIndexExclusive()));
                processor.addWrite(transformer, cap, ()-> {
                    AsmrValueListNode<Integer> list =  new AsmrValueListNode<Integer>();
                    AsmrValueNode<Integer> vn = new AsmrValueNode<>();
                    vn.init(modifier);
                    list.addCopy(vn);
                    return list;
                });
            }
        });
        return this;
    }

    private List<Pair<Integer,AsmrMethodNode>> getMatchingMethods(AsmrClassNode acn) {
        String owner = this.methodOwner;
        String name = this.methodName;
        String desc = this.methodDesc;
        List<Pair<Integer,AsmrMethodNode>> list = new ArrayList<>();
        for(int i = 0; i < acn.methods().size(); i++) {
            AsmrMethodNode amn = acn.methods().get(i);
            if(matchesMethod(amn,owner,name,desc)) {
                list.add(Pair.create(i, amn));
            }
        }
        // if(list.isEmpty()) throw new RuntimeException(); // Not sure why this was here, but past me seems to thing is was important. See getMatch
        return list;
    }

    public UnifiedBuilder method_replace(AsmrMethodNode node) {
        this.withClassTarget(acn-> {
            // Pair<Integer, AsmrMethodNode> pair = getMatch(acn);
            List<Pair<Integer, AsmrMethodNode>> pairs = getMatchingMethods(acn);
            for(Pair<Integer, AsmrMethodNode> pair : pairs) {
                AsmrSliceCapture<AsmrMethodNode> cap = processor.refCapture(acn.methods(), before(pair.left), after(pair.left));
                processor.addWrite(transformer, cap, ()-> {
                    AsmrMethodListNode list = new AsmrMethodListNode();
                    list.addCopy(node);
                    return list;
                });
                return;
            }
        });
        return this;
    }

    public UnifiedBuilder method_replaceBody(AsmrMethodBodyNode node) {
        this.withClassTarget(acn-> {
            // Pair<Integer, AsmrMethodNode> pair = getMatch(acn);
            List<Pair<Integer, AsmrMethodNode>> pairs = getMatchingMethods(acn);
            for(Pair<Integer, AsmrMethodNode> pair : pairs) {
                AsmrNodeCapture<AsmrMethodBodyNode> cap = processor.refCapture(pair.right.body());
                processor.addWrite(transformer, cap, ()-> {
                    return node;
                });
            }
        });
        return this;
    }

    public UnifiedBuilder method_inject(AsmrMethodNode node) {
        this.withClassTarget(acn-> {
            int index = before(acn.methods().size()-1);
            AsmrSliceCapture<AsmrMethodNode> cap = processor.refCapture(acn.methods(), index, index);
            processor.addWrite(transformer, cap, ()-> {
                AsmrMethodListNode list = new AsmrMethodListNode();
                list.addCopy(node);
                return list;
            });
            return;
        });
        return this;
    }

    private void un_initMethod() {
        // this.methodTarget = null;
        this.methodName = null;
        this.methodOwner = null;
        this.methodDesc = null;
    }

    public UnifiedBuilder doneWithMethod() {
        un_initMethod();
        un_initInsns();
        return this;
    }
    //#endregion

    //#region Field
    String fieldOwner;
    String fieldName;
    String fieldDesc;

    public UnifiedBuilder initBuilderField(String owner, String name, String desc) {
        this.fieldOwner = owner;
        this.fieldName = name;
        this.fieldDesc = desc;
        return this;
    }

    public UnifiedBuilder field_transformDesc(String desc) {
        this.withClassTarget(acn-> {
            for(int i = 0; i < acn.fields().size(); i++) {
                AsmrFieldNode amn = acn.fields().get(i);
                AsmrNodeCapture<AsmrValueNode<String>> cap = processor.refCapture(amn.desc());
                processor.addWrite(transformer, cap, ()-> {
                    AsmrValueNode<String> node = new AsmrValueNode<>();
                    node.init(desc);
                    return node;
                });
            }
        });
        return this;
    }

    public UnifiedBuilder field_transformName(String name) {
        this.withClassTarget(acn-> {
            for(int i = 0; i < acn.fields().size(); i++) {
                AsmrFieldNode amn = acn.fields().get(i);
                AsmrNodeCapture<AsmrValueNode<String>> cap = processor.refCapture(amn.name());
                processor.addWrite(transformer, cap, ()-> {
                    AsmrValueNode<String> node = new AsmrValueNode<>();
                    node.init(name);
                    return node;
                });
            }
        });
        return this;
    }

    public UnifiedBuilder field_removeModifier(int modify) {
        String owner = this.fieldOwner;
        String name = this.fieldName;
        String desc = this.fieldDesc;
        this.withClassTarget(acn-> {
            for(int method = 0; method < acn.fields().size(); method++) {
                AsmrFieldNode afn = acn.fields().get(method);
                if(!fieldMatches(afn,owner,name,desc)) continue;
                for(int mod = 0; mod < afn.modifiers().size(); mod++) {
                    AsmrValueNode<Integer> modifier = afn.modifiers().get(mod);
                    if(modify == modifier.value().intValue()) {
                        AsmrSliceCapture<AsmrValueNode<Integer>> cap = processor.refCapture(afn.modifiers(), before(mod), after(mod));
                        System.out.println(String.format("removing: %s %s %s", owner, name, desc));
                        System.out.println(String.format("start(%s) end(%s)", cap.startIndexInclusive(), cap.endIndexExclusive()));
                        processor.addWrite(transformer, cap, ()-> {
                            return new AsmrValueListNode<>();
                        });
                    }
                }
            }
        });
        return this;
    }

    public UnifiedBuilder field_addModifier(int modifier) {
        String owner = this.fieldOwner;
        String name = this.fieldName;
        String desc = this.fieldDesc;
        this.withClassTarget(acn-> {
            for(int i = 0; i < acn.fields().size(); i++) {
                AsmrFieldNode afn = acn.fields().get(i);
                if(!fieldMatches(afn,owner,name,desc)) continue;
                if(afn.modifiers().stream().mapToInt(AsmrValueNode::value).filter(val->val == modifier).findAny().isPresent()) {
                    return;
                }
                AsmrSliceCapture<AsmrValueNode<Integer>> cap = getModifierAdditionSlice(processor, afn.modifiers(), modifier);
                System.out.println(String.format("Adding: %s %s %s", owner, name, desc));
                System.out.println(String.format("start(%s) end(%s)", cap.startIndexInclusive(), cap.endIndexExclusive()));
                processor.addWrite(transformer, cap, ()-> {
                    AsmrValueListNode<Integer> list =  new AsmrValueListNode<Integer>();
                    AsmrValueNode<Integer> vn = new AsmrValueNode<>();
                    vn.init(modifier);
                    list.addCopy(vn);
                    return list;
                });
            }
        });
        return this;
    }

    private Pair<Integer,AsmrFieldNode> getMatchingField(AsmrClassNode acn) {
        String owner = this.fieldOwner;
        String name = this.fieldName;
        String desc = this.fieldDesc;
        for(int i = 0; i < acn.fields().size(); i++) {
            AsmrFieldNode afn = acn.fields().get(i);
            if(fieldMatches(afn,owner,name,desc)) {
                return Pair.create(i, afn); 
            }
        }
        throw new RuntimeException();
    }

    public UnifiedBuilder field_replace(AsmrFieldNode node) {
        this.withClassTarget(acn-> {
            Pair<Integer, AsmrFieldNode> pair = getMatchingField(acn);
            AsmrNodeCapture<AsmrFieldNode> cap = processor.refCapture(pair.right);
            processor.addWrite(transformer, cap, ()-> {
                return node;
            });
            return;
        });
        return this;
    }

    public UnifiedBuilder field_inject(AsmrFieldNode node) {
        this.withClassTarget(acn-> {
            int index = before(acn.fields().size()-1);
            AsmrSliceCapture<AsmrFieldNode> cap = processor.refCapture(acn.fields(), index, index);
            processor.addWrite(transformer, cap, ()-> {
                AsmrFieldListNode list = new AsmrFieldListNode();
                list.addCopy(node);
                return list;
            });
            return;
        });
        return this;
    }

    private void un_initField() {
        // this.fieldTarget = null;
        this.fieldDesc = null;
        this.fieldOwner = null;
        this.fieldName = null;
    }

    public UnifiedBuilder doneWithField() {
        this.un_initField();
        return this;
    }
    //#endregion

    //#region Insn
    Object[][] insns;

    public UnifiedBuilder initBuilderInsn(Object[][] insns) {
        // insnTarget = new InsnsTarget(insns);
        this.insns = insns;
        return this;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public UnifiedBuilder insn_replace(AsmrInstructionListNode<?> node) {
        Object[][] insns = this.insns;
        this.withClassTarget(acn-> {
            // Pair<Integer, AsmrMethodNode> pair = builder.getMatch(acn);
            List<Pair<Integer, AsmrMethodNode>> pairs = this.getMatchingMethods(acn);
            for(Pair<Integer, AsmrMethodNode> pair : pairs) {
                List<Pair<Integer, Integer>> list = findSliceIndicesForInsns(pair.right, insns);
                for(Pair<Integer, Integer> indices : list) {
                    AsmrSliceCapture cap = processor.refCapture(pair.right.body().instructions(), before(indices.left), after(indices.right));
                    processor.addWrite(this.transformer, cap, ()-> {
                        return node;
                    });
                }
            }
        });
        return this;
    }
    @SuppressWarnings({"unchecked", "rawtypes"})
    public UnifiedBuilder insn_replace(int ordinal, AsmrInstructionListNode<?> node) {
        Object[][] insns = this.insns;
        this.withClassTarget(acn-> {
            // Pair<Integer, AsmrMethodNode> pair = builder.getMatch(acn);
            List<Pair<Integer, AsmrMethodNode>> pairs = this.getMatchingMethods(acn);
            for(Pair<Integer, AsmrMethodNode> pair : pairs) {
                Pair<Integer, Integer> list = findSliceIndicesForInsns(pair.right,insns).get(ordinal);

                AsmrSliceCapture cap = processor.refCapture(pair.right.body().instructions(), before(list.left), after(list.right));
                processor.addWrite(this.transformer, cap, ()-> {
                    return node;
                });
            }
        });
        return this;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public UnifiedBuilder insn_injectBefore(Object[][] newInsns) {
        Object[][] insns = this.insns;
        this.withClassTarget(acn-> {
            // Pair<Integer, AsmrMethodNode> pair = builder.getMatch(acn);
            List<Pair<Integer, AsmrMethodNode>> pairs = this.getMatchingMethods(acn);
            for(Pair<Integer, AsmrMethodNode> pair : pairs) {
                List<Pair<Integer, Integer>> list = findSliceIndicesForInsns(pair.right,insns);
                for(Pair<Integer, Integer> indices : list) {
                    AsmrSliceCapture cap = processor.refCapture(pair.right.body().instructions(), before(indices.left), before(indices.left));
                    processor.addWrite(this.transformer, cap, ()-> {
                        return toInsns(newInsns, pair.right);
                        // return Insn.toAsmrInsnListNode(node, pair.right.body());
                    });
                }
            }
        });
        return this;
    }
    @SuppressWarnings({"unchecked", "rawtypes"})
    public UnifiedBuilder insn_injectBefore(int ordinal, Object[][] newInsns) {
        Object[][] insns = this.insns;
        this.withClassTarget(acn-> {
            // Pair<Integer, AsmrMethodNode> pair = builder.getMatch(acn);
            List<Pair<Integer, AsmrMethodNode>> pairs = this.getMatchingMethods(acn);
            for(Pair<Integer, AsmrMethodNode> pair : pairs) {
                Pair<Integer, Integer> list = findSliceIndicesForInsns(pair.right,insns).get(ordinal);
                AsmrSliceCapture cap = processor.refCapture(pair.right.body().instructions(), before(list.left), before(list.left));
                processor.addWrite(this.transformer, cap, ()-> {
                    return toInsns(newInsns, pair.right);
                    // return Insn.toAsmrInsnListNode(node, pair.right.body());
                });
            }
        });
        return this;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public UnifiedBuilder insn_injectAfter(Object[][] newInsns) {
        Object[][] insns = this.insns;
        this.withClassTarget(acn-> {
            // Pair<Integer, AsmrMethodNode> pair = builder.getMatch(acn);
            List<Pair<Integer, AsmrMethodNode>> pairs = this.getMatchingMethods(acn);
            for(Pair<Integer, AsmrMethodNode> pair : pairs) {
                List<Pair<Integer, Integer>> list = findSliceIndicesForInsns(pair.right, insns);
                for(Pair<Integer, Integer> indices : list) {
                    AsmrSliceCapture cap = processor.refCapture(pair.right.body().instructions(), after(indices.right), after(indices.right));
                    processor.addWrite(this.transformer, cap, ()-> {
                        return toInsns(newInsns, pair.right);
                        // return Insn.toAsmrInsnListNode(node, pair.right.body());
                    });
                }
            }
        });
        return this;
    }
    @SuppressWarnings({"unchecked", "rawtypes"})
    public UnifiedBuilder insn_injectAfter(int ordinal, Object[][] newInsns) {
        Object[][] insns = this.insns;
        this.withClassTarget(acn-> {
            // Pair<Integer, AsmrMethodNode> pair = builder.getMatch(acn);
            List<Pair<Integer, AsmrMethodNode>> pairs = this.getMatchingMethods(acn);
            for(Pair<Integer, AsmrMethodNode> pair : pairs) {
                Pair<Integer, Integer> list = findSliceIndicesForInsns(pair.right, insns).get(ordinal);

                AsmrSliceCapture cap = processor.refCapture(pair.right.body().instructions(), after(list.right), after(list.right));
                processor.addWrite(this.transformer, cap, ()-> {
                    return toInsns(newInsns, pair.right);
                    // return Insn.toAsmrInsnListNode(node, pair.right.body());
                });
            }
        });
        return this;
    }
    static final String BEFORE = "BEFORE";
    static final String AFTER = "AFTER";
    private static boolean isBefore(String s) {
        return s.toUpperCase().equals(BEFORE);
    }
    public UnifiedBuilder insn_inject(String beforeOrAfter, Object[][] newInsns) {
        return isBefore(beforeOrAfter) ? insn_injectBefore(newInsns) : insn_injectAfter(newInsns);
    }
    public UnifiedBuilder insn_inject(int ordinal, String beforeOrAfter, Object[][] newInsns) {
        return isBefore(beforeOrAfter) ? insn_injectBefore(ordinal, newInsns) : insn_injectAfter(ordinal, newInsns);
    }

    public UnifiedBuilder insn_inject(Object[][] newInsns) {
        return insn_injectBefore(newInsns);
    }

    public UnifiedBuilder insn_replaceConstant(Integer o) {
        return null;
    }
    public UnifiedBuilder insn_replaceConstant(Long o) {
        return null;
    }
    public UnifiedBuilder insn_replaceConstant(String o) {
        return null;
    }
    public UnifiedBuilder insn_replaceConstant(Float o) {
        return null;
    }
    public UnifiedBuilder insn_replaceConstant(Double o) {
        return null;
    }

    private void replaceFieldMethodStuff(String s, Function<AsmrMethodInsnNode,AsmrValueNode<String>> method, Function<AsmrFieldInsnNode,AsmrValueNode<String>> field) {
        if(this.insns.length > 1) throw new RuntimeException();
        String type;
        if(methodInsnNode.contains(this.insns[0][0])) {
            type = "m";
        } else if(fieldInsnNode.contains(this.insns[0][0])) {
            type = "f";
        } else {
            throw new RuntimeException();
        }
        Object[][] insns = this.insns;
        this.withClassTarget(acn-> {
            // Pair<Integer, AsmrMethodNode> pair = builder.getMatch(acn);
            List<Pair<Integer, AsmrMethodNode>> pairs = this.getMatchingMethods(acn);
            for(Pair<Integer, AsmrMethodNode> pair : pairs) {
                List<AsmrAbstractInsnNode<?>> list = findInsns(pair.right, insns);
                for(AsmrAbstractInsnNode<?> insnNode : list) {
                    AsmrNodeCapture<AsmrValueNode<String>> cap;
                    if(type.equals("m")) {
                        cap = processor.refCapture(method.apply((AsmrMethodInsnNode) insnNode));
                    } else {
                        cap = processor.refCapture(field.apply((AsmrFieldInsnNode) insnNode));
                    }
                    processor.addWrite(this.transformer, cap, ()-> {
                        AsmrValueNode<String> node = new AsmrValueNode<>();
                        node.init(s);
                        return node;
                    });
                }
            }
        });
    }

    public UnifiedBuilder insn_replaceOwner(String newOwner) {
        this.replaceFieldMethodStuff(newOwner, AsmrMethodInsnNode::owner, AsmrFieldInsnNode::owner);
        return this;
    }
    public UnifiedBuilder insn_replaceName(String newName) {
        this.replaceFieldMethodStuff(newName, AsmrMethodInsnNode::name, AsmrFieldInsnNode::name);
        return this;
    }
    public UnifiedBuilder insn_replaceDesc(String newDesc) {
        this.replaceFieldMethodStuff(newDesc, AsmrMethodInsnNode::desc, AsmrFieldInsnNode::desc);
        return this;
    }

    public UnifiedBuilder insn_replaceIntOperand(int newIntOperand) {
        if(this.insns.length > 1) throw new RuntimeException();
        if(!insnInList(this.insns[0][0],intInsnNode)) throw new RuntimeException();
        Object[][] insns = this.insns;
        this.withClassTarget(acn-> {
            // Pair<Integer, AsmrMethodNode> pair = builder.getMatch(acn);
            
            List<Pair<Integer, AsmrMethodNode>> pairs = this.getMatchingMethods(acn);
            for(Pair<Integer, AsmrMethodNode> pair : pairs) {
                List<AsmrAbstractInsnNode<?>> list = findInsns(pair.right, insns);
                for(AsmrAbstractInsnNode<?> insnNode : list) {
                    AsmrNodeCapture<AsmrValueNode<Integer>> cap = processor.refCapture(((AsmrIntInsnNode)insnNode).operand());
                    processor.addWrite(this.transformer, cap, ()-> {
                        AsmrValueNode<Integer> node = new AsmrValueNode<>();
                        node.init(newIntOperand);
                        return node;
                    });
                }
            }
        });
        return this;
    }

    public UnifiedBuilder insn_replaceType(String newType) {
        if(this.insns.length > 1) throw new RuntimeException();
        if(!insnInList(this.insns[0][0], typeInsnNode)) throw new RuntimeException();
        Object[][] insns = this.insns;
        this.withClassTarget(acn-> {
            List<Pair<Integer, AsmrMethodNode>> pairs = this.getMatchingMethods(acn);
            for(Pair<Integer, AsmrMethodNode> pair : pairs) {
                List<AsmrAbstractInsnNode<?>> list = findInsns(pair.right, insns);
                for(AsmrAbstractInsnNode<?> insnNode : list) {
                    AsmrNodeCapture<AsmrValueNode<String>> cap = processor.refCapture(((AsmrTypeInsnNode)insnNode).desc());
                    processor.addWrite(this.transformer, cap, ()-> {
                        AsmrValueNode<String> node = new AsmrValueNode<>();
                        node.init(newType);
                        return node;
                    });
                }
            }
        });
        return this;
    }
    public UnifiedBuilder insn_replaceVar(int newVarIndex) {
        if(this.insns.length > 1) throw new RuntimeException();
        if(!insnInList(this.insns[0][0], varInsnNode)) throw new RuntimeException();
        Object[][] insns = this.insns;
        this.withClassTarget(acn-> {
            List<Pair<Integer, AsmrMethodNode>> pairs = this.getMatchingMethods(acn);
            for(Pair<Integer, AsmrMethodNode> pair : pairs) {
                List<AsmrAbstractInsnNode<?>> list = findInsns(pair.right, insns);
                for(AsmrAbstractInsnNode<?> insnNode : list) {
                    AsmrNodeCapture<AsmrValueNode<AsmrIndex>> cap = processor.refCapture(((AsmrVarInsnNode)insnNode).varIndex());
                    processor.addWrite(this.transformer, cap, ()-> {
                        return pair.right.body().localIndexes().get(newVarIndex);
                    });
                }
            }
        });
        return this;
    }

    private void un_initInsns() {
        // this.insnTarget = null;
        this.insns = null;
    }

    public UnifiedBuilder doneWithInsn() {
        un_initInsns();
        return this;
    }
    //#endregion

	@SuppressWarnings({ "unchecked" })
	private static <T extends AsmrNode<?>> T parentOf(Class<T> clazz, AsmrNode<?> target) {
		while (target != null) {
			target = target.parent();
			if (clazz.isInstance(target))
				return (T) target;
		}
		return null;
	}

    //#region FieldTargetMethods
    private static boolean field_skipName(String fieldName) {
        return fieldName == skip;
    }
    private static boolean field_skipOwner(String fieldOwner) {
        return fieldOwner == skip;
    }
    private static boolean field_skipDesc(String fieldDesc) {
        return fieldDesc == skip;
    }
    private static boolean fieldMatches(AsmrNode<?> obj, String fieldOwner, String fieldName, String fieldDesc) {
        if(obj instanceof AsmrFieldNode) {
            AsmrFieldNode node = (AsmrFieldNode) obj;
            boolean 
                matchname = node.name().value().equals(fieldName) || field_skipName(fieldName),
                matchdesc = node.desc().value().equals(fieldDesc) || field_skipDesc(fieldDesc),
                matchowner = (node.parent() != null && parentOf(AsmrClassNode.class, node).name().value().equals(fieldOwner)) || field_skipOwner(fieldOwner);
            return matchdesc && matchname && matchowner;
        } else if(obj instanceof AsmrFieldInsnNode) {
            AsmrFieldInsnNode node = (AsmrFieldInsnNode) obj;
            boolean 
                matchname = node.name().value().equals(fieldName) || field_skipName(fieldName),
                matchdesc = node.desc().value().equals(fieldDesc) || field_skipDesc(fieldDesc),
                matchowner = node.owner().value().equals(fieldOwner) || field_skipOwner(fieldOwner);
            return matchdesc && matchname && matchowner;
        }
        throw new RuntimeException();
    }
    //#endregion

    //#region MethodtargetMethods
    private static boolean method_skipName(String methodName) {
        return methodName == skip;
    }
    private static boolean method_skipOwner(String methodOwner) {
        return methodOwner == skip;
    }
    private static boolean method_skipDesc(String methodDesc) {
        return methodDesc == skip;
    }
    private static boolean matchesMethod(AsmrNode<?> obj, String methodOwner, String methodName, String methodDesc) {
        if(obj instanceof AsmrMethodNode) {
            AsmrMethodNode node = (AsmrMethodNode) obj;
            boolean 
                matchname = node.name().value().equals(methodName) || method_skipName(methodName),
                matchdesc = node.desc().value().equals(methodDesc) || method_skipDesc(methodDesc),
                matchowner = (node.parent() != null && parentOf(AsmrClassNode.class, node).name().value().equals(methodOwner)) || method_skipOwner(methodOwner);
            return matchdesc && matchname && matchowner;
        } else if(obj instanceof AsmrMethodInsnNode) {
            AsmrMethodInsnNode node = (AsmrMethodInsnNode) obj;

            boolean 
                matchname = node.name().value().equals(methodName) || method_skipName(methodName),
                matchdesc = node.desc().value().equals(methodDesc) || method_skipDesc(methodDesc),
                matchowner = node.owner().value().equals(methodOwner) || method_skipOwner(methodOwner);
            return matchdesc && matchname && matchowner;
        }
        throw new RuntimeException("Failed");
    }    
    //#endregion

    //#region Insn
    public static Object[] fieldInsn(int opcode, String owner, String name, String desc) {
        return new Object[]{opcode,owner,name,desc};
    }
    public static Object[] methodInsn(int opcode, String owner, String name, String desc) {
        return new Object[]{opcode,owner,name,desc};
    }
    public static Object[] intInsn(int opcode, int operand) {
        return new Object[]{opcode,operand};
    }
    public static Object[] typeInsn(int opcode, String desc) {
        return new Object[]{opcode,desc};
    }
    public static Object[] varInsn(int opcode, int var) {
        return new Object[]{opcode,var};
    }
    //#endregion
    public static final Map<String,Integer> nameToOpcode = ((Supplier<Map<String,Integer>>)()-> {
        Map<String,Integer> map = new HashMap<>();
        map.put("NOP",0);
        map.put("ACONST_NULL",1);
        map.put("ICONST_M1",2);
        map.put("ICONST_0",3);
        map.put("ICONST_1",4);
        map.put("ICONST_2",5);
        map.put("ICONST_3",6);
        map.put("ICONST_4",7);
        map.put("ICONST_5",8);
        map.put("LCONST_0",9);
        map.put("LCONST_1",10);
        map.put("FCONST_0",11);
        map.put("FCONST_1",12);
        map.put("FCONST_2",13);
        map.put("DCONST_0",14);
        map.put("DCONST_1",15);
        map.put("BIPUSH",16);
        map.put("SIPUSH",17);
        map.put("LDC",18);
        map.put("ILOAD",21);
        map.put("LLOAD",22);
        map.put("FLOAD",23);
        map.put("DLOAD",24);
        map.put("ALOAD",25);
        map.put("IALOAD",46);
        map.put("LALOAD",47);
        map.put("FALOAD",48);
        map.put("DALOAD",49);
        map.put("AALOAD",50);
        map.put("BALOAD",51);
        map.put("CALOAD",52);
        map.put("SALOAD",53);
        map.put("ISTORE",54);
        map.put("LSTORE",55);
        map.put("FSTORE",56);
        map.put("DSTORE",57);
        map.put("ASTORE",58);
        map.put("IASTORE",79);
        map.put("LASTORE",80);
        map.put("FASTORE",81);
        map.put("DASTORE",82);
        map.put("AASTORE",83);
        map.put("BASTORE",84);
        map.put("CASTORE",85);
        map.put("SASTORE",86);
        map.put("POP",87);
        map.put("POP2",88);
        map.put("DUP",89);
        map.put("DUP_X1",90);
        map.put("DUP_X2",91);
        map.put("DUP2",92);
        map.put("DUP2_X1",93);
        map.put("DUP2_X2",94);
        map.put("SWAP",95);
        map.put("IADD",96);
        map.put("LADD",97);
        map.put("FADD",98);
        map.put("DADD",99);
        map.put("ISUB",100);
        map.put("LSUB",101);
        map.put("FSUB",102);
        map.put("DSUB",103);
        map.put("IMUL",104);
        map.put("LMUL",105);
        map.put("FMUL",106);
        map.put("DMUL",107);
        map.put("IDIV",108);
        map.put("LDIV",109);
        map.put("FDIV",110);
        map.put("DDIV",111);
        map.put("IREM",112);
        map.put("LREM",113);
        map.put("FREM",114);
        map.put("DREM",115);
        map.put("INEG",116);
        map.put("LNEG",117);
        map.put("FNEG",118);
        map.put("DNEG",119);
        map.put("ISHL",120);
        map.put("LSHL",121);
        map.put("ISHR",122);
        map.put("LSHR",123);
        map.put("IUSHR",124);
        map.put("LUSHR",125);
        map.put("IAND",126);
        map.put("LAND",127);
        map.put("IOR",128);
        map.put("LOR",129);
        map.put("IXOR",130);
        map.put("LXOR",131);
        map.put("IINC",132);
        map.put("I2L",133);
        map.put("I2F",134);
        map.put("I2D",135);
        map.put("L2I",136);
        map.put("L2F",137);
        map.put("L2D",138);
        map.put("F2I",139);
        map.put("F2L",140);
        map.put("F2D",141);
        map.put("D2I",142);
        map.put("D2L",143);
        map.put("D2F",144);
        map.put("I2B",145);
        map.put("I2C",146);
        map.put("I2S",147);
        map.put("LCMP",148);
        map.put("FCMPL",149);
        map.put("FCMPG",150);
        map.put("DCMPL",151);
        map.put("DCMPG",152);
        map.put("IFEQ",153);
        map.put("IFNE",154);
        map.put("IFLT",155);
        map.put("IFGE",156);
        map.put("IFGT",157);
        map.put("IFLE",158);
        map.put("IF_ICMPEQ",159);
        map.put("IF_ICMPNE",160);
        map.put("IF_ICMPLT",161);
        map.put("IF_ICMPGE",162);
        map.put("IF_ICMPGT",163);
        map.put("IF_ICMPLE",164);
        map.put("IF_ACMPEQ",165);
        map.put("IF_ACMPNE",166);
        map.put("GOTO",167);
        map.put("JSR",168);
        map.put("RET",169);
        map.put("TABLESWITCH",170);
        map.put("LOOKUPSWITCH",171);
        map.put("IRETURN",172);
        map.put("LRETURN",173);
        map.put("FRETURN",174);
        map.put("DRETURN",175);
        map.put("ARETURN",176);
        map.put("RETURN",177);
        map.put("GETSTATIC",178);
        map.put("PUTSTATIC",179);
        map.put("GETFIELD",180);
        map.put("PUTFIELD",181);
        map.put("INVOKEVIRTUAL",182);
        map.put("INVOKESPECIAL",183);
        map.put("INVOKESTATIC",184);
        map.put("INVOKEINTERFACE",185);
        map.put("INVOKEDYNAMIC",186);
        map.put("NEW",187);
        map.put("NEWARRAY",188);
        map.put("ANEWARRAY",189);
        map.put("ARRAYLENGTH",190);
        map.put("ATHROW",191);
        map.put("CHECKCAST",192);
        map.put("INSTANCEOF",193);
        map.put("MONITORENTER",194);
        map.put("MONITOREXIT",195);
        map.put("MULTIANEWARRAY",197);
        map.put("IFNULL",198);
        map.put("IFNONNULL",199); 
        return map;
    }).get();
    private static final Map<Integer,String> opcodeToName = ((Supplier<Map<Integer,String>>)()-> {
        Map<Integer,String> map = new HashMap<>();
        for(Entry<String, Integer> i : nameToOpcode.entrySet()) map.put(i.getValue(), i.getKey());
        return map;
    }).get();
    private static List<String> newArrayList(String... strings) {
        List<String> list = new ArrayList<>();
        for(String s : strings) list.add(s);
        return list;
    }
    private boolean insnInList(Object o, List<String> list) {
        if(o instanceof Integer) {
            return list.contains(opcodeToName.get(o));
        }
        throw new RuntimeException();
    }
    private final static List<String> 
        insnNode = newArrayList("NOP", "ACONST_NULL", "ICONST_M1", "ICONST_0", "ICONST_1", "ICONST_2", "ICONST_3", "ICONST_4", "ICONST_5", "LCONST_0", "LCONST_1", "FCONST_0", "FCONST_1", "FCONST_2", "DCONST_0", "DCONST_1", "IALOAD", "LALOAD", "FALOAD", "DALOAD", "AALOAD", "BALOAD", "CALOAD", "SALOAD", "IASTORE", "LASTORE", "FASTORE", "DASTORE", "AASTORE", "BASTORE", "CASTORE", "SASTORE", "POP", "POP2", "DUP", "DUP_X1", "DUP_X2", "DUP2", "DUP2_X1", "DUP2_X2", "SWAP", "IADD", "LADD", "FADD", "DADD", "ISUB", "LSUB", "FSUB", "DSUB", "IMUL", "LMUL", "FMUL", "DMUL", "IDIV", "LDIV", "FDIV", "DDIV", "IREM", "LREM", "FREM", "DREM", "INEG", "LNEG", "FNEG", "DNEG", "ISHL", "LSHL", "ISHR", "LSHR", "IUSHR", "LUSHR", "IAND", "LAND", "IOR", "LOR", "IXOR", "LXOR", "IINC", "I2L", "I2F", "I2D", "L2I", "L2F", "L2D", "F2I", "F2L", "F2D", "D2I", "D2L", "D2F", "I2B", "I2C", "I2S", "LCMP", "FCMPL", "FCMPG", "DCMPL", "DCMPG", "IRETURN", "LRETURN", "FRETURN", "DRETURN", "ARETURN", "RETURN", "ARRAYLENGTH"),
        varInsnNode = newArrayList("ILOAD", "LLOAD", "FLOAD", "DLOAD", "ALOAD", "ISTORE", "LSTORE", "FSTORE", "DSTORE", "ASTORE"),
        fieldInsnNode = newArrayList("GETSTATIC","PUTSTATIC","GETFIELD","PUTFIELD"),
        methodInsnNode = newArrayList("INVOKEVIRTUAL", "INVOKESPECIAL", "INVOKESTATIC", "INVOKEINTERFACE"),
        typeInsnNode = newArrayList("NEW", "ANEWARRAY", "CHECKCAST", "INSTANCEOF"),
        intInsnNode = newArrayList("BIPUSH", "SIPUSH", "NEWARRAY"),
        // invokedynamic = Lists.newArrayList("INVOKEDYNAMIC"),
        // lookupSwitch = Lists.newArrayList("LOOKUPSWITCH"),
        // tableSwitch = Lists.newArrayList("TABLESWITCH"),
        jump = newArrayList("IFNE", "IFLT", "IFGE", "IFGT", "IFLE", "IF_ICMPEQ", "IF_ICMPNE", "IF_ICMPLT", "IF_ICMPGE", "IF_ICMPGT", "IF_ICMPLE", "IF_ACMPEQ", "IF_ACMPNE", "GOTO", "IFNULL", "IFNONNULL")
        // java7 = newArrayList("JSR", "RET"),
        // lvVarInsn = newArrayList("LVLOAD","LVSTORE","LVLOAD_ILLEGAL","LVSTORE_ILLEGAL")
        ;
    ;
    private static boolean matchesInsn(AsmrAbstractInsnNode<?> node, Object[] insn) {
        String name = opcodeToName.get(insn[0]);
        if(insnNode.contains(name) && node instanceof AsmrNoOperandInsnNode) {
            return ((AsmrNoOperandInsnNode)node).opcode().value().equals(insn[0]);
        } else if(varInsnNode.contains(name) && node instanceof AsmrVarInsnNode) {
            AsmrVarInsnNode vnode = (AsmrVarInsnNode) node;
            AsmrMethodBodyNode method = parentOf(AsmrMethodBodyNode.class, node);
            int index = 0;
            for(; index < method.localIndexes().size(); index++) {
                if(method.localIndexes().get(index).equals(vnode.varIndex())) {
                    break;
                }
            }
            return vnode.opcode().value().equals(insn[0]) &&
                ((Integer)insn[1]).intValue() == index;
        } else if(fieldInsnNode.contains(name) && node instanceof AsmrFieldInsnNode) {
            AsmrFieldInsnNode field = (AsmrFieldInsnNode) node;
            boolean opcode = field.opcode().value().equals(insn[0]);
            boolean owner = field.owner().value().equals(insn[1]);
            boolean Bname = field.name().value().equals(insn[2]);
            boolean desc = field.desc().value().equals(insn[3]);
            return opcode && owner && Bname && desc;
        } else if(methodInsnNode.contains(name) && node instanceof AsmrMethodInsnNode) {
            AsmrMethodInsnNode method = (AsmrMethodInsnNode) node;
            boolean opcode = method.opcode().value().equals(insn[0]);
            boolean owner = method.owner().value().equals(insn[1]);
            boolean Bname = method.name().value().equals(insn[2]);
            boolean desc = method.desc().value().equals(insn[3]);
            return opcode && owner && Bname && desc;
        } else if(typeInsnNode.contains(name) && node instanceof AsmrTypeInsnNode) {
            AsmrTypeInsnNode type = (AsmrTypeInsnNode) node;
            return type.opcode().value().equals(insn[0]) && type.desc().value().equals(insn[1]);
        } else if(intInsnNode.contains(name) && node instanceof AsmrIntInsnNode) {
            AsmrIntInsnNode type = (AsmrIntInsnNode) node;
            return type.opcode().value().equals(insn[0]) && type.operand().value().equals(insn[1]);
        } else if(jump.contains(name) && node instanceof AsmrJumpInsnNode) {
            AsmrJumpInsnNode jump = (AsmrJumpInsnNode) node;
            AsmrMethodBodyNode method = parentOf(AsmrMethodBodyNode.class, node);
            int index = 0;
            for(;index < method.localIndexes().size();index++) {
                if(method.localIndexes().get(index).equals(jump.label())) {
                    break;
                }
            }
            return jump.opcode().value().equals(insn[0]) && index == ((Integer)insn[1]).intValue(); 
        } else {
            throw new RuntimeException();
        }
    }

    @SuppressWarnings({"rawtypes","unchecked"})
    private static AsmrInstructionListNode<?> toInsns(Object[][] insns, AsmrMethodNode method) {
        AsmrInstructionListNode list = new AsmrInstructionListNode();
        for(Object[] insn : insns) {
            list.addCopy(toInsn(insn, method));
        }
        return list;
    }

    private static AsmrAbstractInsnNode<?> toInsn(Object[] insn, AsmrMethodNode method) {
        String name = opcodeToName.get(insn[0]);
        if(insnNode.contains(name)) {
            AsmrNoOperandInsnNode node = new AsmrNoOperandInsnNode();
            node.opcode().init((Integer) insn[0]);
            return node;
        } else if(varInsnNode.contains(name)) {
            AsmrVarInsnNode node = new AsmrVarInsnNode();
            node.opcode().init((Integer) insn[0]);
            AsmrLocalVariableNode local = method.body().localVariables().get((Integer)insn[1]);
            node.varIndex().init(local.index().value());
            return node;
        } else if(fieldInsnNode.contains(name)) {
            AsmrFieldInsnNode node = new AsmrFieldInsnNode();
            node.opcode().init((Integer) insn[0]);
            node.owner().init((String) insn[1]);
            node.name().init((String) insn[2]);
            node.desc().init((String) insn[3]);
            return node;
        } else if(methodInsnNode.contains(name)) {
            AsmrMethodInsnNode node = new AsmrMethodInsnNode();
            node.opcode().init((Integer) insn[0]);
            node.owner().init((String) insn[1]);
            node.name().init((String) insn[2]);
            node.desc().init((String) insn[3]);
            return node;
        } else if(typeInsnNode.contains(name)) {
            AsmrTypeInsnNode node = new AsmrTypeInsnNode();
            node.opcode().init((Integer) insn[0]);
            node.desc().init((String) insn[1]);
            return node;
        } else if(intInsnNode.contains(name)) {
            AsmrIntInsnNode node = new AsmrIntInsnNode();
            node.opcode().init((Integer) insn[0]);
            node.operand().init((Integer) insn[1]);
            return node;
        } else if(jump.contains(name)) {
            AsmrJumpInsnNode node = new AsmrJumpInsnNode();
            node.opcode().init((Integer) insn[0]);
            AsmrValueNode<AsmrIndex> index = method.body().localIndexes().get((Integer) insn[1]);
            node.label().init(index.value());
            return node;
        } else {
            throw new RuntimeException();
        }
    }
    @SuppressWarnings({"unchecked"})
    private static List<Pair<Integer, Integer>> findSliceIndicesForInsns(AsmrMethodNode node, Object[][] insns) {
        List<Pair<Integer, Integer>> indices = new ArrayList<>();
        Iterator<AsmrAbstractInsnNode<? extends AsmrNode<?>>> iter = (Iterator<AsmrAbstractInsnNode<? extends AsmrNode<?>>>) node.body().instructions().iterator();
        while(iter.hasNext()) {
            AsmrAbstractInsnNode<?> insn = iter.next();
            Iterator<Object[]> matchInsns = Stream.of(insns).iterator();
            while(matchInsns.hasNext()) {
                Object[] matchInsn = matchInsns.next();
                if (!matchesInsn(insn, matchInsn)) break;
                if (!matchInsns.hasNext()) {
                    int index = indexOf(insn, node.body().instructions());
                    indices.add(Pair.create(index, index + insns.length));
                    // indices.add(indexOf(insn, node.body().instructions()) - (position == Position.AFTER ? 0 : opcodeList.size()));
                }
            }
        }
        return indices;
    }
    private static int indexOf(AsmrNode<?> node , AsmrAbstractListNode<?,?> list) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) == node) {
                return i;
            } 
        }
        return -1;
    }

    @SuppressWarnings({"unchecked"})
    private static List<AsmrAbstractInsnNode<?>> findInsns(AsmrMethodNode node, Object[][] insns) {
        List<AsmrAbstractInsnNode<?>> indices = new ArrayList<>();
        Iterator<AsmrAbstractInsnNode<? extends AsmrNode<?>>> iter = (Iterator<AsmrAbstractInsnNode<? extends AsmrNode<?>>>) node.body().instructions().iterator();
        while(iter.hasNext()) {
            AsmrAbstractInsnNode<?> insn = iter.next();
            Iterator<Object[]> matchInsns = Stream.of(insns).iterator();
            while(matchInsns.hasNext()) {
                Object[] matchInsn = matchInsns.next();
                if (!matchesInsn(insn, matchInsn)) break;
                if (!matchInsns.hasNext()) {
                    indices.add(insn);
                }
            }
        }
        return indices;
    }
}
