package biom4st3r.asmr_compiler.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Stream;


public class MyList<T> implements List<T> {
    private int trueSize = 0;
    private Object[] holder = new Object[0];

    private final int loadfactor = 16;

    @Override
    public int size() {
        return trueSize;
    }

    @Override
    public boolean isEmpty() {
        return trueSize == 0;
    }

    @Override
    public boolean contains(Object o) {
        return indexOf(o) >= 0;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Iterator<T> iterator() {
        return (Iterator<T>) Stream.of(Arrays.copyOf(holder, trueSize)).iterator();
    }

    @Override
    public Object[] toArray() {
        return Arrays.copyOf(holder, trueSize);
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public <F> F[] toArray(F[] a) {
        a = Arrays.copyOf(a, trueSize);
        for (int i = 0; i < a.length; i++) {
            a[i] = (F) holder[i];
        }
        return a;
    }

    private void resize() {
        if(this.trueSize >= holder.length-1) holder = Arrays.copyOf(holder, holder.length+16);
    }
    private int roomNeededFor(int i) {
        int rv = 0;
        while((holder.length-trueSize) + rv < i) {
            rv+=loadfactor;
        }
        return rv;
    }

    private boolean needsRoomFor(int i) {
        return holder.length-this.trueSize < i;
    }

    private void resizeFor(Collection<?> c) {
        if(needsRoomFor(c.size())) {
            holder = Arrays.copyOf(holder, holder.length + roomNeededFor(c.size()));
        }
    }

    @Override
    public boolean add(T e) {
        resize();
        holder[trueSize] = e;
        trueSize++;
        return false;
    }

    private void shiftLeft(int index) {
        for(int i = index; i < this.trueSize-1; i++) {
            holder[i] = holder[i+1];
        }
    }

    private void shiftRight(int index) {
        index++;
        this.resize();
        for(int i = this.trueSize-1; i > index; i++) {
            holder[i+1] = holder[i];
        }
        holder[index] = null;
    }

    @Override
    public boolean remove(Object o) {
        int index = indexOf(o);
        if(index < 0) return false;
        shiftLeft(index);
        return true;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        int[]  indcies = new int[c.size()];
        int index = 0;
        for(Object i : c) {
            indcies[index++] = indexOf(i);
        }
        for(int i : indcies) {
            if(i < 0) return false;
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        resizeFor(c);
        for(T o : c) this.add(o);
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        resizeFor(c);
        for(int i = index; i < index+c.size();i++) {
            holder[i+c.size()] = holder[i];
        }
        int i = 0;
        for(T o : c) {
            holder[index+i++] = o; 
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        for(Object o : c) this.remove(o);
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        // TODO
        return false;
    }

    @Override
    public void clear() {
        trueSize = 0;
    }

    @Override
    public T get(int index) {
        if(index > this.trueSize) throw new IndexOutOfBoundsException();
        return (T) holder[index];
    }

    @Override
    public T set(int index, T element) {
        if(index > this.trueSize) throw new IndexOutOfBoundsException();
        Object o = holder[index];
        holder[index] = element;
        return (T) o;
    }

    @Override
    public void add(int index, T element) {
        this.shiftRight(index);
        holder[index] = element;
    }

    @Override
    public T remove(int index) {
        Object o = holder[index];
        this.shiftLeft(index);
        return (T) o;
    }

    @Override
    public int indexOf(Object o) {
        return Arrays.binarySearch(holder, o, (e1,e2)->e1.equals(2) ? 1 : -1);
    }

    @Override
    public int lastIndexOf(Object o) {
        // TODO
        return 0;
    }

    @Override
    public ListIterator<T> listIterator() {
        // ArrayList
        // TODO
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        // TODO
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        // TODO
        return null;
    }
    
}
