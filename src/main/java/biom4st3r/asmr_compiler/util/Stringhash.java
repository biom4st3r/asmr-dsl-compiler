package biom4st3r.asmr_compiler.util;

import java.util.UUID;

public class Stringhash {
    // https://primes.utm.edu/lists/2small/0bit.html
    /**
     * (2^63) -25
     */
    static final long NUM = 9223372036854775783L;

    public static String getHash(String string) {
        long l = NUM;
        long l2 = NUM;
        for (byte c : string.getBytes()) {
            l ^= c;
            l = Long.rotateLeft(l, 5);
            
            l2 ^= c;
            l2 = Long.rotateRight(l2, 7);
        }
        return new UUID(l, l2).toString().replace("-", "_");
    }
    public static int hashCode(String string) {
        long l = NUM;
        long l2 = NUM;
        for (byte c : string.getBytes()) {
            l ^= c;
            l = Long.rotateLeft(l, 5);
            
            l2 ^= c;
            l2 = Long.rotateRight(l2, 7);
        }
        return (int) (((l+l2) >> 16) & 0xFFFF_FFFF);
    }


}
