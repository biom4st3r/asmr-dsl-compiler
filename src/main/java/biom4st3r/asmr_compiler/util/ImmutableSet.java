package biom4st3r.asmr_compiler.util;

import java.util.Collection;
import java.util.HashSet;
import java.util.function.Predicate;

public class ImmutableSet<T> extends HashSet<T> {
    private boolean locked = false;
    private void testLock() {
        if(locked) {
            throw new IllegalStateException();
        }
    }
    @Override
    public boolean add(T e) {
        testLock();
        return super.add(e);
    }
    @Override
    public boolean addAll(Collection<? extends T> c) {
        testLock();
        return super.addAll(c);
    }
    @Override
    public boolean removeAll(Collection<?> c) {
        testLock();
        return super.removeAll(c);
    }
    @Override
    public boolean retainAll(Collection<?> c) {
        testLock();
        return super.retainAll(c);
    }
    @Override
    public boolean remove(Object o) {
        testLock();
        return super.remove(o);
    }
    @Override
    public boolean removeIf(Predicate<? super T> filter) {
        testLock();
        return super.removeIf(filter);
    }
    public void lock() {
        locked = true;
    }
}
