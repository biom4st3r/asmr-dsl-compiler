package biom4st3r.asmr_compiler.util;

public class TextBuilder {
    StringBuilder builder = new StringBuilder();
    public TextBuilder newLine() {
        builder.append('\n');
        return this;
    }
    public TextBuilder space() {
        builder.append(' ');
        return this;
    }
    public TextBuilder sep() {
        builder.append(' ');
        return this;
    }

    public TextBuilder append(CharSequence seq) {
        builder.append(seq);
        return this;
    }
    public TextBuilder append(String seq) {
        builder.append(seq);
        return this;
    }
    public TextBuilder append(char[] seq) {
        builder.append(seq);
        return this;
    }
    public TextBuilder append(Object seq) {
        builder.append(seq);
        return this;
    }
    public TextBuilder append(boolean seq) {
        builder.append(seq);
        return this;
    }
    public TextBuilder append(int seq) {
        builder.append(seq);
        return this;
    }
    public TextBuilder append(long seq) {
        builder.append(seq);
        return this;
    }
    public TextBuilder append(short seq) {
        builder.append(seq);
        return this;
    }
    public TextBuilder append(byte seq) {
        builder.append(seq);
        return this;
    }
    public TextBuilder append(float seq) {
        builder.append(seq);
        return this;
    }
    public TextBuilder append(double seq) {
        builder.append(seq);
        return this;
    }
    @Override
    public String toString() {
        return this.builder.toString();
    }
}
