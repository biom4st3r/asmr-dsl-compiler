package biom4st3r.asmr_compiler.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Predicate;

import biom4st3r.asmr_compiler.Main;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;
import org.quiltmc.asmr.processor.AsmrPlatform;
import org.quiltmc.asmr.processor.AsmrProcessor;
import org.quiltmc.asmr.tree.AsmrAbstractListNode;
import org.quiltmc.asmr.tree.AsmrNode;
import org.quiltmc.asmr.tree.AsmrValueListNode;
import org.quiltmc.asmr.tree.AsmrValueNode;
import org.quiltmc.asmr.tree.member.AsmrClassNode;
import org.quiltmc.asmr.util.AsmrClassWriter;

public class AsmrUtil {

	public static final <T> AsmrValueNode<T> contains(AsmrValueListNode<T> list, T obj) {
		for (AsmrValueNode<T> node : list) {
			if (node.value().equals(obj))
				return node;
		}
		return null;
	}

	public static final <T extends AsmrNode<?>> T contains(AsmrAbstractListNode<? extends T, ?> list,
			Predicate<T> pred) {
		for (T t : list) {
			if (pred.test(t))
				return t;
		}
		return null;
	}

	public static final AsmrPlatform plateform = new AsmrPlatform() {
		@Override
		public byte[] getClassBytecode(String arg0) throws ClassNotFoundException {
			return AsmrUtil.findClassBytes(Class.forName(arg0.replace("/", ".")));
		}
	};

	static AsmrProcessor processor = null;

	public static AsmrProcessor getProcessor() {
		if (processor != null)
			return processor;
		System.gc();
		AsmrProcessor processor = new AsmrProcessor(plateform);
		try {
			List<String> classes = Lists.newArrayList(
				"net/minecraft/server/PlayerManager",
				"net/minecraft/server/TestEnum",
				"net/minecraft/server/AccessTest"
			);
			for (String s : classes) {
				processor.addClass(s, plateform.getClassBytecode(s));
			}
			// processor.addJar(Path.of("libs", "minecraft-1.16.5-mapped-build.6-v2.jar"),
			// "724579B8A7B924CC26D4A36E838DE02E2658FB78");
			// File f = new File("libs_aof");
			// for (File file : f.listFiles()) {
			// if (file.getName().endsWith(".jar")) {
			// processor.addJar(file.toPath(), null);
			// }
			// }

		} catch (Throwable e) {
			e.printStackTrace();
		}
		System.gc();
		AsmrUtil.processor = processor;
		return processor;
	}

	public static byte[] findClassBytes(Class<?> clazz) {
		Path sourceLocation;
		try {
			sourceLocation = Paths.get(clazz.getProtectionDomain().getCodeSource().getLocation().toURI());
		} catch (URISyntaxException e) {
			e.printStackTrace();
			throw new NoClassDefFoundError(clazz.getName());
		}

		byte[] bytes;
		try {
			if (Files.isDirectory(sourceLocation)) {
				Path classFile = sourceLocation.resolve(clazz.getName().replace('.', File.separatorChar) + ".class");
				bytes = Files.readAllBytes(classFile);
			} else {
				try (FileSystem fs = FileSystems.newFileSystem(sourceLocation, (ClassLoader) null);) {
					Path jarEntry = fs.getPath(clazz.getName().replace('.', '/') + ".class");
					bytes = Files.readAllBytes(jarEntry);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw new NoClassDefFoundError(clazz.getName());
		}

		return bytes;
	}

	// public static Class getClass(AsmrClassNode node) {
	// ClassWriter cw = new
	// org.objectweb.asm.ClassWriter(org.objectweb.asm.ClassWriter.COMPUTE_FRAMES);
	// node.accept(cw);
	// ClassLoader loader = new ClassLoader(){};
	// Thread.currentThread().setContextClassLoader(loader);
	// Method defineClass;
	// try {
	// defineClass = ClassLoader.class.getDeclaredMethod("defineClass",
	// byte[].class, int.class, int.class);
	// defineClass.setAccessible(true);
	// Class clazz = (Class) defineClass.invoke(loader, cw.toByteArray(), 0,
	// cw.toByteArray().length);
	// return clazz;
	// } catch (NoSuchMethodException | SecurityException | IllegalAccessException |
	// IllegalArgumentException | InvocationTargetException e) {
	// e.printStackTrace();
	// }
	// return null;
	// }

	public static Class<?> defineClass(AsmrProcessor processor, AsmrClassNode classNode) {
		String className = classNode.name().value().replace('/', '.');

		ClassWriter writer = new AsmrClassWriter(processor);
		classNode.accept(writer);
		byte[] bytecode = writer.toByteArray();

		ClassLoader classLoader = new ClassLoader(Thread.currentThread().getContextClassLoader()) {
			private Class<?> customClass = null;

			@Override
			protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
				Class<?> loadedClass = findLoadedClass(name);
				if (loadedClass == null) {
					try {
						loadedClass = findClass(name);
					} catch (ClassNotFoundException ignore) {
						return super.loadClass(name, resolve);
					}
				}

				if (resolve) {
					resolveClass(loadedClass);
				}
				return loadedClass;
			}

			@Override
			protected Class<?> findClass(String name) throws ClassNotFoundException {
				if (className.equals(name)) {
					if (customClass == null) {
						customClass = defineClass(className, bytecode, 0, bytecode.length);
					}
					return customClass;
				}
				return super.findClass(name);
			}
		};
		try {
			return classLoader.loadClass(className);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	@SuppressWarnings({ "unchecked" })
	public static <T extends AsmrNode<?>> T parentOf(Class<T> clazz, AsmrNode<?> target) {
		while (target != null) {
			target = target.parent();
			if (clazz.isInstance(target))
				return (T) target;
		}
		return null;
	}

	public static void toFile(String className, String fileName, AsmrProcessor processor) {
		AsmrClassWriter cw = new AsmrClassWriter(processor);
		ClassVisitor cv = new ClassVisitor(Opcodes.ASM8, cw) {
			@Override
			public void visit(int version, int access, String name, String signature, String superName,
					String[] interfaces) {
				super.visit(Opcodes.V1_8, access, name, signature, superName, interfaces);
			}
		};
		AsmrClassNode acn = processor.findClassImmediately(className);
		// new CheckClassAdapter(cv, false)
		acn.accept(cv);
		toFile(cw.toByteArray(), fileName);
	}

	public static void toFile(byte[] bytes, String fileName) {
		if(!Main.doWrites) {
			return;
		}
		File f = new File(fileName + ".class");
		FileOutputStream stream;
		try {
			stream = new FileOutputStream(f);
			stream.write(bytes);
			stream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
