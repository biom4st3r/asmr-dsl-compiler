package biom4st3r.asmr_compiler;

public class ClCl extends ClassLoader {
    private static ClCl INSTANCE;

    public static final ClCl GET_INSTANCE() {
        return INSTANCE;
    }
    static {
        INSTANCE = new ClCl(Thread.currentThread().getContextClassLoader());
        Thread.currentThread().setContextClassLoader(INSTANCE);
    }
    public ClCl(ClassLoader contextClassLoader) {
        super(contextClassLoader);
    }

    @SuppressWarnings({"deprecation","unchecked"})
    public <T> Class<T> defineClass(byte[] b) {
        return (Class<T>) this.defineClass(b, 0, b.length);
    }
}
