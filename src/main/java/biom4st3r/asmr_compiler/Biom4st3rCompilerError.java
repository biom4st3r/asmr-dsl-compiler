package biom4st3r.asmr_compiler;

public class Biom4st3rCompilerError extends RuntimeException {
    public Biom4st3rCompilerError(String error, Object... args) {
        super(String.format(error, args));
    }
}