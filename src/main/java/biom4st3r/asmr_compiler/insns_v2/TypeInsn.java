package biom4st3r.asmr_compiler.insns_v2;

import biom4st3r.asmr_compiler.ConverterDoDad;
import org.quiltmc.asmr.tree.AsmrValueNode;
import org.quiltmc.asmr.tree.insn.AsmrAbstractInsnNode;
import org.quiltmc.asmr.tree.insn.AsmrTypeInsnNode;
import org.quiltmc.asmr.tree.method.AsmrMethodBodyNode;

public class TypeInsn implements Insn {
    public final int opcode;
    public final String desc;

    public TypeInsn(int opcode, String desc) {
        this.opcode = opcode;
        this.desc = desc;
    }

    private boolean descMatches(AsmrValueNode<String> s) {
        return this.desc.equals(s.value()) || Insn.SKIP.equals(this.desc);
    }
    private boolean opcodeMatches(AsmrValueNode<Integer> i) {
        return this.opcode == i.value().intValue() || this.opcode == -1;
    }

    @Override
    public boolean matches(AsmrAbstractInsnNode<?> node) {
        if (node instanceof AsmrTypeInsnNode tn) {
            return opcodeMatches(tn.opcode()) && descMatches(tn.desc());
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder
            .append(ConverterDoDad.opcodeToName.get(this.opcode)).append(' ')
            .append(this.desc)
            ;
        return builder.toString();
    }

    @Override
    public AsmrAbstractInsnNode<?> as(AsmrMethodBodyNode body) {
        AsmrTypeInsnNode insn = new AsmrTypeInsnNode();
        insn.desc().init(this.desc);
        insn.opcode().init(this.opcode);
        return insn;
    }
}
