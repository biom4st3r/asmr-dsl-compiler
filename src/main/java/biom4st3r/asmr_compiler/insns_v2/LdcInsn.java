package biom4st3r.asmr_compiler.insns_v2;

import org.objectweb.asm.Opcodes;
import org.quiltmc.asmr.tree.AsmrValueNode;
import org.quiltmc.asmr.tree.insn.AsmrAbstractInsnNode;
import org.quiltmc.asmr.tree.insn.AsmrConstantList;
import org.quiltmc.asmr.tree.insn.AsmrLdcInsnNode;
import org.quiltmc.asmr.tree.method.AsmrMethodBodyNode;

public class LdcInsn implements Insn {
    public final Object cst;

    public LdcInsn(String cst) {
        this.cst = cst;
    }
    public LdcInsn(Integer cst) {
        this.cst = cst;
    }
    public LdcInsn(Float cst) {
        this.cst = cst;
    }
    public LdcInsn(Double cst) {
        this.cst = cst;
    }
    public LdcInsn(Long cst) {
        this.cst = cst;
    }

    @Override
    public boolean matches(AsmrAbstractInsnNode<?> node) {
        if (node instanceof AsmrLdcInsnNode ldc) {
            return ldc.cstList().get(0).equals(this.cst);
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder
            .append("LDC").append(' ')
            .append(this.cst)
            ;
        return builder.toString();
    }

    @Override
    @SuppressWarnings({"rawtypes","unchecked"})
    public AsmrAbstractInsnNode<?> as(AsmrMethodBodyNode body) {
        AsmrLdcInsnNode insn = new AsmrLdcInsnNode();
        ((AsmrConstantList)insn.cstList()).addCopy(new AsmrValueNode<Object>().init(cst));
        // insn.cstList().addCopy(new AsmrValueNode<Object>(insn)); // TODO
        insn.opcode().init(Opcodes.LDC);
        return insn;
    }
}
