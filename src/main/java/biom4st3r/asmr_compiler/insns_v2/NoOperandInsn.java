package biom4st3r.asmr_compiler.insns_v2;

import biom4st3r.asmr_compiler.ConverterDoDad;
import org.quiltmc.asmr.tree.AsmrValueNode;
import org.quiltmc.asmr.tree.insn.AsmrAbstractInsnNode;
import org.quiltmc.asmr.tree.insn.AsmrNoOperandInsnNode;
import org.quiltmc.asmr.tree.method.AsmrMethodBodyNode;

public class NoOperandInsn implements Insn {

    private final int opcode;

    public NoOperandInsn(int opcode) {
        this.opcode = opcode;
    }

    private boolean opcodeMatches(AsmrValueNode<Integer> i) {
        return this.opcode == i.value().intValue() || this.opcode == -1;
    }

    @Override
    public boolean matches(AsmrAbstractInsnNode<?> node) {
        if (node instanceof AsmrNoOperandInsnNode non) {
            return opcodeMatches(non.opcode());
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder
            .append(ConverterDoDad.opcodeToName.get(this.opcode))
            ;
        return builder.toString();
    }

    @Override
    public AsmrAbstractInsnNode<?> as(AsmrMethodBodyNode body) {
        AsmrNoOperandInsnNode insn = new AsmrNoOperandInsnNode();
        insn.opcode().init(this.opcode);
        return insn;
    }
    
}

