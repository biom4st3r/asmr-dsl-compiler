package biom4st3r.asmr_compiler.insns_v2;

import java.util.List;

import org.quiltmc.asmr.tree.insn.AsmrAbstractInsnNode;
import org.quiltmc.asmr.tree.insn.AsmrInstructionListNode;
import org.quiltmc.asmr.tree.member.AsmrClassNode;
import org.quiltmc.asmr.tree.member.AsmrMethodNode;
import org.quiltmc.asmr.tree.method.AsmrMethodBodyNode;

public interface Insn {
    public static final String SKIP = "#SKIP";
    boolean matches(AsmrAbstractInsnNode<?> node);
    AsmrAbstractInsnNode<?> as(AsmrMethodBodyNode body);

    @Override
    String toString();

    static AsmrMethodNode getMethodNode(AsmrMethodBodyNode node) {
        return (AsmrMethodNode) node.parent();
    }

    static AsmrClassNode getClassNode(AsmrMethodBodyNode node) {
        return (AsmrClassNode) node.parent().parent().parent();
    }
    
    @SuppressWarnings({"rawtypes","unchecked"})
    static AsmrInstructionListNode<?> toAsmrInsnListNode(List<Insn> list, AsmrMethodBodyNode body) {
        AsmrInstructionListNode asmrlist = new AsmrInstructionListNode<>();
        list.stream().map(insn->insn.as(body)).forEach(asmrlist::addCopy);
        return asmrlist;
    }
}
