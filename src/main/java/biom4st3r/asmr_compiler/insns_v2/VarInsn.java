package biom4st3r.asmr_compiler.insns_v2;

import biom4st3r.asmr_compiler.ConverterDoDad;
import biom4st3r.asmr_compiler.util.AsmrUtil;
import org.quiltmc.asmr.tree.insn.AsmrAbstractInsnNode;
import org.quiltmc.asmr.tree.insn.AsmrVarInsnNode;
import org.quiltmc.asmr.tree.member.AsmrMethodNode;
import org.quiltmc.asmr.tree.method.AsmrMethodBodyNode;

public class VarInsn implements Insn {
    public final int opcode;
    public final int var;

    public VarInsn(int opcode, int var) {
        this.opcode = opcode;
        this.var = var;
    }

    @Override
    public boolean matches(AsmrAbstractInsnNode<?> node) {
        if (node instanceof AsmrVarInsnNode vn) {
            AsmrMethodNode method = AsmrUtil.parentOf(AsmrMethodNode.class, node);
            if (method.body().localIndexes().size() < var + 1) return false;
            boolean matchesVar = vn.varIndex().value().equals(method.body().localIndexes().get(var).value());
            return vn.opcode().value() == this.opcode && matchesVar;
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder
            .append(ConverterDoDad.opcodeToName.get(this.opcode)).append(' ')
            .append(this.var)
            ;
        return builder.toString();
    }

    @Override
    public AsmrAbstractInsnNode<?> as(AsmrMethodBodyNode body) {
        AsmrVarInsnNode insn = new AsmrVarInsnNode();
        insn.varIndex().init(body.localIndexes().get(this.var).value());
        insn.opcode().init(this.opcode);
        return insn;
    }

}
