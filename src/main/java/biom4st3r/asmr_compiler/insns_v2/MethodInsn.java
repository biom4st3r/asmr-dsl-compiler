package biom4st3r.asmr_compiler.insns_v2;

import biom4st3r.asmr_compiler.ConverterDoDad;
import org.objectweb.asm.Opcodes;
import org.quiltmc.asmr.tree.AsmrValueNode;
import org.quiltmc.asmr.tree.insn.AsmrAbstractInsnNode;
import org.quiltmc.asmr.tree.insn.AsmrMethodInsnNode;
import org.quiltmc.asmr.tree.method.AsmrMethodBodyNode;

public class MethodInsn implements Insn {
    public final int opcode;
    public final String owner;
    public final String name;
    public final String desc;

    public MethodInsn(int opcode, String owner, String name, String desc) {
        this.opcode = opcode;
        this.owner = owner;
        this.name = name;
        this.desc = desc;
    }
    
    private boolean nameMatches(AsmrValueNode<String> s) {
        return this.name.equals(s.value()) || Insn.SKIP.equals(this.name);
    }
    private boolean ownerMatches(AsmrValueNode<String> s) {
        return this.owner.equals(s.value()) || Insn.SKIP.equals(this.owner);
    }
    private boolean descMatches(AsmrValueNode<String> s) {
        return this.desc.equals(s.value()) || Insn.SKIP.equals(this.desc);
    }
    private boolean opcodeMatches(AsmrValueNode<Integer> i) {
        return this.opcode == i.value().intValue() || this.opcode == -1;
    }

    @Override
    public boolean matches(AsmrAbstractInsnNode<?> node) {
        if (node instanceof AsmrMethodInsnNode mn) {
            return nameMatches(mn.name()) && ownerMatches(mn.owner()) && descMatches(mn.desc()) && opcodeMatches(mn.opcode());
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder
            .append(ConverterDoDad.opcodeToName.get(this.opcode)).append(' ')
            .append(owner).append(' ').append(this.name).append(' ').append(this.desc);
            ;
        return builder.toString();
    }

    @Override
    public AsmrAbstractInsnNode<?> as(AsmrMethodBodyNode body) {
        AsmrMethodInsnNode insn = new AsmrMethodInsnNode();
        insn.name().init(this.name);
        insn.owner().init(this.owner);
        insn.desc().init(this.desc);
        insn.opcode().init(this.opcode);
        insn.itf().init(this.opcode == Opcodes.INVOKEINTERFACE);
        return insn;
    }
    
}
