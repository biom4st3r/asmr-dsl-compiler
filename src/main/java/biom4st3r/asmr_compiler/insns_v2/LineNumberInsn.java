package biom4st3r.asmr_compiler.insns_v2;

import org.quiltmc.asmr.tree.insn.AsmrAbstractInsnNode;
import org.quiltmc.asmr.tree.insn.AsmrLineNumberNode;
import org.quiltmc.asmr.tree.method.AsmrMethodBodyNode;

public class LineNumberInsn implements Insn {

    @Override
    public boolean matches(AsmrAbstractInsnNode<?> node) {
        return false;
    }

    @Override
    public AsmrAbstractInsnNode<?> as(AsmrMethodBodyNode body) {
        return new AsmrLineNumberNode();
    }
    
}
