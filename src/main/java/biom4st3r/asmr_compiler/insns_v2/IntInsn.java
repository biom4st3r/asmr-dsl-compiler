package biom4st3r.asmr_compiler.insns_v2;

import biom4st3r.asmr_compiler.ConverterDoDad;
import org.quiltmc.asmr.tree.AsmrValueNode;
import org.quiltmc.asmr.tree.insn.AsmrAbstractInsnNode;
import org.quiltmc.asmr.tree.insn.AsmrIntInsnNode;
import org.quiltmc.asmr.tree.method.AsmrMethodBodyNode;

public class IntInsn implements Insn {
    public final int opcode;
    public final int operand;

    public IntInsn(int opcode, int operand) {
        this.opcode = opcode;
        this.operand = operand;
    }


    private boolean operandMatches(AsmrValueNode<Integer> i) {
        return this.operand == i.value().intValue() || this.operand == -1;
    }
    private boolean opcodeMatches(AsmrValueNode<Integer> i) {
        return this.opcode == i.value().intValue() || this.opcode == -1;
    }

    @Override
    public boolean matches(AsmrAbstractInsnNode<?> node) {
        if (node instanceof AsmrIntInsnNode in) {
            return opcodeMatches(in.opcode()) && operandMatches(in.operand());
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder
            .append(ConverterDoDad.opcodeToName.get(this.opcode)).append(' ')
            .append(this.operand)
            ;
        return builder.toString();
    }

    @Override
    public AsmrAbstractInsnNode<?> as(AsmrMethodBodyNode body) {
        AsmrIntInsnNode insn = new AsmrIntInsnNode();
        insn.operand().init(this.operand);
        insn.opcode().init(this.opcode);
        return insn;
    }
    
}
