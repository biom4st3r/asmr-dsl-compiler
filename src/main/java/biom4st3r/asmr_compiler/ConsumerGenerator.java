package biom4st3r.asmr_compiler;

import biom4st3r.asmr_compiler.OpcodeMethodVisitor.OpcodeClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Type;

import static org.objectweb.asm.Opcodes.*;

import java.util.function.Consumer;

public class ConsumerGenerator {
    public static ClassWriter generate(Class<?> clazz) {
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
        OpcodeClassVisitor cv = OpcodeMethodVisitor.newCv(cw);
        cv.visit(V1_8, ACC_PUBLIC|ACC_FINAL, "biom4st3r/asm/Consumer$"+clazz.getName(), null, Type.getInternalName(Object.class), new String[]{Type.getInternalName(Consumer.class)});
        
        OpcodeMethodVisitor mv = cv.visitMethod(ACC_PUBLIC, "accept", String.format("(%s)V", Type.getDescriptor(clazz)), null, null);
        mv.visitAnnotation(Type.getDescriptor(Override.class), true);
        mv.visitCode();

        mv.RETURN();
        mv.visitEnd();
        cv.visitEnd(); return cw;
    }
}
