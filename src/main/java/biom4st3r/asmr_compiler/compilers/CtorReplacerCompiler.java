package biom4st3r.asmr_compiler.compilers;

import java.util.Iterator;
import java.util.List;

import biom4st3r.asmr_compiler.Biom4st3rCompilerError;
import biom4st3r.asmr_compiler.compilers.descriptions.CtorReplacementDesc;
import biom4st3r.asmr_compiler.compilers.utils.Pair;
import biom4st3r.asmr_compiler.compilers.utils.Tree;

public class CtorReplacerCompiler extends Compiler {
    public static CtorReplacementDesc builder(String sourcefile, List<Tree> trees) {
        if(!Transformers.v0_REPLACE_CTOR.matches(trees.get(0))) throw new Biom4st3rCompilerError("Transformer description is not valid for access transformer: %s", trees.get(0).getJoinedInfo());

        enactMacros(trees);
        Pair<String,String> pair = new Pair<String,String>(null, null);
        Iterator<Tree> iter = trees.stream().filter(Insns.CLASS::matches).iterator();
        while(iter.hasNext()) {
            Tree tree = iter.next();
            if (Insns.CLASS_TARGET.matches(tree)) {
                pair = pair.withLeft(tree.info[2]);
            } else if (Insns.CLASS_REPLACER.matches(tree)) {
                pair = pair.withRight(tree.info[2]);
            }
            if(pair.isFull()) break;
        }
        CtorReplacementDesc desc = new CtorReplacementDesc(sourcefile, pair.left, pair.right);
        // TODO Make check if this is a valid method?
        return desc;
    }
}
