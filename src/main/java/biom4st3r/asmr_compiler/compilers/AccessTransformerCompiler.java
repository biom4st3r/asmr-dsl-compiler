package biom4st3r.asmr_compiler.compilers;

import static org.objectweb.asm.Opcodes.*;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import biom4st3r.asmr_compiler.Biom4st3rCompilerError;
import biom4st3r.asmr_compiler.compilers.descriptions.AccessTransformerDesc;
import biom4st3r.asmr_compiler.compilers.utils.AccessTarget;
import biom4st3r.asmr_compiler.compilers.utils.Tree;
import biom4st3r.asmr_compiler.compilers.utils.AccessTarget.TargetType;
import biom4st3r.asmr_compiler.util.Lists;
import biom4st3r.asmr_compiler.util.Maps;

public class AccessTransformerCompiler extends Compiler {
    public static final Map<String, Integer> map = ((Supplier<Map<String, Integer>>)()-> {
        Map<String, Integer> map = Maps.newHashMap();
        map.put("ABSTRACT", ACC_ABSTRACT);
        map.put("PUBLIC", ACC_PUBLIC);
        map.put("PRIVATE", ACC_PRIVATE);
        map.put("FINAL", ACC_FINAL);
        map.put("STRICT", ACC_STRICT);
        map.put("PROTECTED", ACC_PROTECTED);
        map.put("SYNCHRONIZED", ACC_SYNCHRONIZED);
        map.put("SYNTHETIC", ACC_SYNTHETIC);
        map.put("VOLATILE", ACC_VOLATILE);
        map.put("TRANSIENT", ACC_TRANSIENT);
        return map;
    }).get();

    public static AccessTransformerDesc builder(String sourcefile, List<Tree> trees) {
        if(!Transformers.v0_ACCESS_TRANSFORMER.matches(trees.get(0))) throw new Biom4st3rCompilerError("Transformer description is not valid for access transformer: %s", trees.get(0).getJoinedInfo());

        List<AccessTarget> list = Lists.newArrayList();
        enactMacros(trees);
        trees.stream().filter(Insns.CLASS::matches).forEach(tree-> {
            tree.children.forEach(child->{
                list.add(new AccessTarget(tree.info[1], child.info[0]));
            });
        });
        pruneTrees(trees, Insns.CLASS::matches);
        trees.stream().filter(Insns.METHOD::matches).forEach(tree-> {
            tree.children.forEach(child-> {
                list.add(new AccessTarget(TargetType.METHOD, tree.info[1], tree.info[2], tree.info[3], child.info[0]));
            });
        });
        pruneTrees(trees, Insns.METHOD::matches);
        trees.stream().filter(Insns.FIELD::matches).forEach(tree-> {
            tree.children.forEach(child-> {
                list.add(new AccessTarget(TargetType.FIELD, tree.info[1], tree.info[2], tree.info[3], child.info[0]));
            });
        });
        pruneTrees(trees, Insns.FIELD::matches);
        Collections.shuffle(list);
        AccessTransformerDesc desc = new AccessTransformerDesc(sourcefile, list);
        return desc;
    }
}
