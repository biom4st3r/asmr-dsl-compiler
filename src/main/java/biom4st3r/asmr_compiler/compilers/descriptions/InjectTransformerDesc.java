package biom4st3r.asmr_compiler.compilers.descriptions;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.Stream.Builder;

import biom4st3r.asmr_compiler.ConverterDoDad;
import biom4st3r.asmr_compiler.compilers.templatetransformers.InjectTransformer;
import biom4st3r.asmr_compiler.compilers.templatetransformers.TransformerGenerator;
import biom4st3r.asmr_compiler.compilers.utils.MatchingInfo;
import biom4st3r.asmr_compiler.compilers.utils.MethodTarget;
import biom4st3r.asmr_compiler.insns_v2.Insn;
import biom4st3r.asmr_compiler.util.Lists;
import biom4st3r.asmr_compiler.util.TextBuilder;
import org.quiltmc.asmr.processor.AsmrProcessor;
import org.quiltmc.asmr.processor.AsmrTransformer;
import org.quiltmc.asmr.tree.member.AsmrMethodListNode;
import org.quiltmc.asmr.tree.member.AsmrMethodNode;

public class InjectTransformerDesc implements Description {
    public static int INDEX = 0;
    public final int index;
    public final String sourceFile;
    public final List<String> classTargets = Lists.newArrayList();
    public final List<MatchingInfo> matchingInfo = Lists.newArrayList();
    public final List<MethodTarget> methodTargets = Lists.newArrayList();
    private final List<String> bodyAddition = Lists.newArrayList();
    public boolean modifiesLineNumbers = false;

    public List<Insn> getBodyAddition() {
        return bodyAddition.stream().map((s)->ConverterDoDad.getNodeFromString(s.split("[\t ]"))).collect(Collectors.toList()); // hax
    }

    public Iterable<MethodTarget> matchingTargets(String owner) {
        return ()->methodTargets.stream().filter(mt->mt.ownerMatches(owner)).iterator();
    }

    // public AsmrInstructionListNode getAsmrBodyAdditions(AsmrMethodBodyNode mdn) {
    //     AsmrInstructionListNode node = new AsmrInstructionListNode<>();
    //     for(Insn insn : getBodyAddition()) {
    //         node.addCopy(insn.as(null));
    //     }
    //     return node;
    // }

    public void addInsn(String s) {
        // if (s.split(" ")[0].equals("LINE")) {
        //     this.modifiesLineNumbers = true;
        // }
        bodyAddition.add(s);
    }

    public String internaGetInsn(int i) {
        return bodyAddition.get(i);
    }

    public int getBodySize() {
        return bodyAddition.size();
    }

    public InjectTransformerDesc(String sourceFile) {
        this.sourceFile = sourceFile;
        this.index = INDEX;
        INDEX++;
    }

    public InjectTransformerDesc(String sourceFile,int i) {
        this.sourceFile = sourceFile;
        this.index = 0;
    }

    public void addMethod(String[] info) {
        this.methodTargets.add(new MethodTarget(info));
    }

    public String[] internalGetMethodTarget(int i) {
        return methodTargets.get(i).internalGet();
    }

    public Stream<AsmrMethodNode> getValidMethodStream(AsmrMethodListNode node) {
        Builder<AsmrMethodNode> builder = Stream.builder();
        for (MethodTarget mt : this.methodTargets) {
            node.stream().filter(mt::matches).forEach(builder::accept);
        }
        return builder.build();
    }

    @Override
    public String toString() {
        TextBuilder builder = new TextBuilder();
        builder.append("Class Targets:").newLine();
        for (String s : classTargets) {
            builder.append("    ").append(s).newLine();
        }
        builder.append("Inject").newLine();
        Iterator<MatchingInfo> iter = matchingInfo.iterator();
        for (MatchingInfo info = null; iter.hasNext();) {
            info = iter.next();

            builder.append(info.position.name()).newLine();
            

            for (Insn node : info.getInsns().collect(Collectors.toSet())) {
                builder.append("    ").append(node.toString()).newLine();
            }
            if (iter.hasNext()) builder.append("\n    or").newLine();
        }
        builder.append("In methods matching:").newLine();
        for (MethodTarget node : methodTargets) {
            builder.append("    ").append(node.name).append(node.desc).newLine();
        }
        builder.append("Body:").newLine();
        for (Insn insn : this.getBodyAddition()) {
            builder.append("    ").append(insn.toString()).newLine();
        }
        return builder.toString();
    }

    @Override
    public String getSourceFileName() {
        return this.sourceFile;
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public <T extends AsmrTransformer> Class<T>[] buildTransformerClass(AsmrProcessor processor) {
        DescBundle<?> bundle = TransformerGenerator.getTransformerClass("biom4st3r/asm/injector/Injectiontransformer_"+sourceFile, InjectTransformer.class, this, this.getClass(), processor);
        return new Class[]{Description.loadClass(bundle.className, bundle.fieldName, bundle.desc, bundle.cw)};
    }
}