package biom4st3r.asmr_compiler.compilers.descriptions;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import biom4st3r.asmr_compiler.Biom4st3rCompilerError;
import biom4st3r.asmr_compiler.compilers.templatetransformers.ReplaceObjTransformer;
import biom4st3r.asmr_compiler.compilers.templatetransformers.TransformerGenerator;
import biom4st3r.asmr_compiler.compilers.utils.MethodTarget;
import biom4st3r.asmr_compiler.compilers.utils.Pair;
import biom4st3r.asmr_compiler.compilers.utils.TypeExploder;
import biom4st3r.asmr_compiler.util.AsmrUtil;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.quiltmc.asmr.processor.AsmrProcessor;
import org.quiltmc.asmr.processor.AsmrTransformer;
import org.quiltmc.asmr.tree.insn.AsmrAbstractInsnNode;
import org.quiltmc.asmr.tree.insn.AsmrFieldInsnNode;
import org.quiltmc.asmr.tree.insn.AsmrInstructionListNode;
import org.quiltmc.asmr.tree.insn.AsmrMethodInsnNode;
import org.quiltmc.asmr.tree.insn.AsmrNoOperandInsnNode;
import org.quiltmc.asmr.tree.insn.AsmrTypeInsnNode;
import org.quiltmc.asmr.tree.member.AsmrClassNode;

@SuppressWarnings({"rawtypes", "unchecked"})
public class ObjectReplacementDesc implements Description {
    public final Pair<MethodTarget,MethodTarget>[] targets;
    public final String targetClass;
    public final String replacingClass;
    public final String sourceFile;

    public ObjectReplacementDesc(String sourceFile, String targetClass, String replacingClass, List<Pair<MethodTarget,MethodTarget>> list) {
        this.sourceFile = sourceFile;
        this.targetClass = targetClass;
        this.replacingClass = replacingClass;
        this.targets = list.toArray(Pair[]::new);
    }

    public boolean isCanadate(AsmrAbstractInsnNode node) {
        if (node instanceof AsmrMethodInsnNode && ((AsmrMethodInsnNode)node).owner().value().equals(targetClass)) {
            return true;
        } else if (node instanceof AsmrTypeInsnNode && ((AsmrTypeInsnNode)node).desc().value().equals(targetClass)) {
            return true;
        } else if (node instanceof AsmrFieldInsnNode && ((AsmrFieldInsnNode)node).desc().value().equals(this.getTargetType().getDescriptor())) {
            return true;
        }
        return false;
    }

    public Type getTargetType() {
        return Type.getObjectType(targetClass);
    }
    public Type getReplacingType() {
        return Type.getObjectType(replacingClass);
    }

    public AsmrInstructionListNode getReplacingInsn(AsmrMethodInsnNode node) {
        Optional<Pair<MethodTarget, MethodTarget>> optional = Stream.of(targets).filter((pair)->pair.left.matches(node)).findFirst();
        if (!optional.isPresent()) {
            System.out.println(AsmrUtil.parentOf(AsmrClassNode.class, node).name().value());
            throw new Biom4st3rCompilerError("No matching replacer for %s.%s%s", node.owner().value(),node.name().value(),node.desc().value());
        }
        MethodTarget target = optional.get().left;
        MethodTarget replacer = optional.get().right;

        TypeExploder targetDesc = new TypeExploder(target.desc);
        TypeExploder replacingDesc = new TypeExploder(replacer.desc);
        AsmrInstructionListNode list = new AsmrInstructionListNode<>();

        if (targetDesc.argLength < replacingDesc.argLength) {
            throw new Biom4st3rCompilerError("Not enough arguments in %s.%s%s or %s.%s%s", node.owner().value(),node.name().value(),node.desc().value(), replacer.owner, replacer.name, replacer.desc);
        } else if (targetDesc.argLength > replacingDesc.argLength) { // TODO Make this smarter
            AsmrNoOperandInsnNode POP = new AsmrNoOperandInsnNode(list); 
            POP.opcode().init(Opcodes.POP);
            AsmrNoOperandInsnNode POP2 = new AsmrNoOperandInsnNode(list);
            POP2.opcode().init(Opcodes.POP2);

            for (int i = 0; i < targetDesc.argLength; i++) {
                if (i >= replacingDesc.argLength) {
                    if (targetDesc.argNeedsPop2(i)) {
                        list.addCopy(POP2);
                    } else {
                        list.addCopy(POP);
                        // if (i-1 >= 0 && list.get(i-1) instanceof AsmrNoOperandInsnNode && ((AsmrNoOperandInsnNode)list.get(i-1)).opcode().value() == Opcodes.POP) {
                        //     list.remove(i-1);
                        //     list.addCopy(POP2);
                        // } else 
                    }
                } else if (!replacingDesc.getArgs()[i].equals(targetDesc.getArgs()[i])) {
                    throw new Biom4st3rCompilerError("Failed to swap %s with %s. Imcompatible type at Index %s. %s is not valid for %s", target.name, replacer.name, i, targetDesc.getArgs()[i].getInternalName(), replacingDesc.getArgs()[i].getInternalName());
                }
            }
        }
        AsmrMethodInsnNode newInvoke = new AsmrMethodInsnNode();
        newInvoke.opcode().init(node.opcode().value());
        newInvoke.name().init(replacer.name);
        newInvoke.owner().init(replacer.owner);
        newInvoke.desc().init(replacer.desc);
        newInvoke.itf().init(node.opcode().value() == Opcodes.INVOKEINTERFACE);
        list.addCopy(newInvoke);
        return list;
    }

    @Override
    public String getSourceFileName() {
        return this.sourceFile;
    }

    @Override
    public <T extends AsmrTransformer> Class<T>[] buildTransformerClass(AsmrProcessor processor) {
        DescBundle bundle = TransformerGenerator.getTransformerClass("biom4st3r/asm/injector/ObjectReplacementTransformer_"+sourceFile, ReplaceObjTransformer.class, this, this.getClass(), processor);
        return new Class[]{Description.loadClass(bundle.className, bundle.fieldName, bundle.desc, bundle.cw)};
    }
}
