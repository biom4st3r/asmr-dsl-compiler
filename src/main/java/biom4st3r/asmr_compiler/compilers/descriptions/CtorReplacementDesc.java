package biom4st3r.asmr_compiler.compilers.descriptions;

import java.util.stream.Stream;
import java.util.stream.Stream.Builder;

import biom4st3r.asmr_compiler.compilers.templatetransformers.CtorTransformer;
import biom4st3r.asmr_compiler.compilers.templatetransformers.TransformerGenerator;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.quiltmc.asmr.processor.AsmrProcessor;
import org.quiltmc.asmr.processor.AsmrTransformer;
import org.quiltmc.asmr.tree.insn.AsmrTypeInsnNode;
import org.quiltmc.asmr.tree.member.AsmrClassNode;

public class CtorReplacementDesc implements Description {
	public final String targetClass;
	public final String replacingClass;
    public final String sourceFile;
    public CtorReplacementDesc(String sourcefile, String targetClass, String replacingClass) {
        this.sourceFile = sourcefile;
        this.targetClass = targetClass;
        this.replacingClass = replacingClass;
    }

    public Type getTargetType() {
        return Type.getObjectType(targetClass);
    }
    public Type getReplacingType() {
        return Type.getObjectType(replacingClass);
    }

    @Override
    public String getSourceFileName() {
        return sourceFile;
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public <T extends AsmrTransformer> Class<T>[] buildTransformerClass(AsmrProcessor processor) {
        DescBundle<?> bundle = TransformerGenerator.getTransformerClass("biom4st3r/asm/ctorreplacer/CtorReplacerTransformer_"+sourceFile, CtorTransformer.class, this, this.getClass(), processor);
        return new Class[]{Description.loadClass(bundle.className, bundle.fieldName, bundle.desc, bundle.cw)};
    }

	public Stream<AsmrTypeInsnNode> getTargetedInsns(AsmrClassNode acn) {
        Builder<AsmrTypeInsnNode> builder = Stream.builder();
        acn
            .methods()
            .stream()
            .map(amn ->
                amn.body().instructions().stream()
                    .filter(insn -> insn instanceof AsmrTypeInsnNode)
                    .map(insn -> AsmrTypeInsnNode.class.cast(insn))
                    .filter(insn -> insn.desc().value().equals(this.targetClass) && insn.opcode().value() == Opcodes.NEW)
                )
                .forEach(insns -> insns.forEach(builder::add));
        return builder.build();
	}
}
