package biom4st3r.asmr_compiler.compilers.descriptions;

import org.objectweb.asm.ClassWriter;
import org.quiltmc.asmr.processor.AsmrProcessor;
import org.quiltmc.asmr.processor.AsmrTransformer;

import biom4st3r.asmr_compiler.ClCl;
import biom4st3r.asmr_compiler.util.AsmrUtil;

public interface Description {
    String getSourceFileName();
	<T extends AsmrTransformer> Class<T>[] buildTransformerClass(AsmrProcessor processor);
    static <T extends AsmrTransformer> Class<T> loadClass(String className, String fieldName, Description desc, ClassWriter cw) {
        String[] sec = className.split("/");
        AsmrUtil.toFile(cw.toByteArray(), sec[sec.length-1]);

        Class<T> clazz = ClCl.GET_INSTANCE().defineClass(cw.toByteArray());
        try {
            clazz.getDeclaredField(fieldName).set(null, desc);
        } catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
            throw new RuntimeException();
        }
        
        return clazz;
    }
    static <T extends AsmrTransformer> Class<T> loadClass(DescBundle<?> bundle) {
        String[] sec = bundle.className.split("/");
        AsmrUtil.toFile(bundle.cw.toByteArray(), sec[sec.length-1]);

        Class<T> clazz = ClCl.GET_INSTANCE().defineClass(bundle.cw.toByteArray());
        try {
            clazz.getDeclaredField(bundle.fieldName).set(null, bundle.desc);
        } catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
            throw new RuntimeException();
        }
        
        return clazz;
    }
    public static class DescBundle<D extends Description> {
        public final ClassWriter cw;
        public final String className;
        public final String fieldName;
        public final D desc;
        public DescBundle(ClassWriter cw, String className, String fieldName, D desc) {
            this.cw = cw;
            this.className = className;
            this.fieldName = fieldName;
            this.desc = desc;
        }
        public final <T extends AsmrTransformer> Class<T> build() {
            return loadClass(this);
        }
    }
}
