package biom4st3r.asmr_compiler.compilers.descriptions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.quiltmc.asmr.processor.AsmrProcessor;
import org.quiltmc.asmr.processor.AsmrTransformer;
import biom4st3r.asmr_compiler.compilers.templatetransformers.AccessTransformer;
import biom4st3r.asmr_compiler.compilers.templatetransformers.TransformerGenerator;
import biom4st3r.asmr_compiler.compilers.utils.AccessTarget;

public class AccessTransformerDesc implements Description {
    private final AccessTarget[] ALL;
    public final String sourceName;

    public AccessTarget[] getAll() {
        return Arrays.copyOf(ALL, ALL.length);
    }

    public AccessTransformerDesc(String sourceName, List<AccessTarget> targets) {
        this.sourceName = sourceName;

        targets.sort((e1,e2)->e1.modifier > e2.modifier ? -1 : e1.modifier < e2.modifier ? 1 : 0);
        ALL = targets.toArray(AccessTarget[]::new);
    }

    public boolean clazzHasTargets(String clazz) {
        for (AccessTarget target : ALL) {
            if(target.owner.equals(clazz)) return true;
        }
        return false;
    }


    @Override
    public String getSourceFileName() {
        return sourceName;
    }

    private AccessTransformerDesc[] getCopies() {
        AccessTransformerDesc[] descs = new AccessTransformerDesc[2];
        List<AccessTarget> additions = new ArrayList<>();
        List<AccessTarget> removal = new ArrayList<>();
        for(AccessTarget target : this.ALL) {
            if(target.add) additions.add(target);
            else removal.add(target);
        }
        descs[0] = new AccessTransformerDesc(this.sourceName, additions);
        descs[1] = new AccessTransformerDesc(this.sourceName, removal);
        return descs;
    }

    @Override
    @SuppressWarnings({"unchecked","rawtypes"})
    public <T extends AsmrTransformer> Class<T>[] buildTransformerClass(AsmrProcessor processor) {

        AccessTransformerDesc[] copies = this.getCopies();
        DescBundle<AccessTransformerDesc> bundle_add =    TransformerGenerator.getTransformerClass("biom4st3r/asm/at/AccessTransformerAdditions_" + sourceName, AccessTransformer.class, copies[0], this.getClass(), processor);
        DescBundle<AccessTransformerDesc> bundle_remove = TransformerGenerator.getTransformerClass("biom4st3r/asm/at/AccessTransformerRemoval_" + sourceName, AccessTransformer.class, copies[1], this.getClass(), processor);
        for(AccessTarget i : bundle_add.desc.ALL) if(!i.add) throw new RuntimeException();
        for(AccessTarget i : bundle_remove.desc.ALL) if(i.add) throw new RuntimeException();
        Class addition = bundle_add.desc.ALL.length == 0 ? null : bundle_add.build();
        Class removal = bundle_remove.desc.ALL.length == 0 ? null : bundle_remove.build();

        if(addition == null && removal == null) {
            return new Class[0];
        } else if (addition == null && removal != null) {
            return new Class[]{removal};
        } else if (addition != null && removal == null) {
            return new Class[]{addition};
        } else return new Class[]{addition,removal};

        // return new Class[]{TransformerGenerator.getTransformerClass("biom4st3r/asm/at/AccessTransformerAdditions_" + sourceName, AccessTransformer.class, this, this.getClass(), processor).build()};
    }
}
