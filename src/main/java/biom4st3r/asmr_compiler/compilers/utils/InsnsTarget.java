package biom4st3r.asmr_compiler.compilers.utils;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import biom4st3r.asmr_compiler.insns_v2.Insn;
import biom4st3r.asmr_compiler.util.Lists;
import org.quiltmc.asmr.tree.AsmrAbstractListNode;
import org.quiltmc.asmr.tree.AsmrNode;
import org.quiltmc.asmr.tree.insn.AsmrAbstractInsnNode;
import org.quiltmc.asmr.tree.member.AsmrMethodNode;

public class InsnsTarget {
    public final Insn[] opcodes;

    public InsnsTarget(Insn[] insns) {
        opcodes = insns;
    }

    private static int indexOf(AsmrNode<?> node , AsmrAbstractListNode<?,?> list) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) == node) {
                return i;
            } 
        }
        return -1;
    }

    @SuppressWarnings({"unchecked"})
    public List<AsmrAbstractInsnNode<?>> findNode(AsmrMethodNode node) {
        List<AsmrAbstractInsnNode<?>> indices = Lists.newArrayList();
        Iterator<AsmrAbstractInsnNode<? extends AsmrNode<?>>> iter = (Iterator<AsmrAbstractInsnNode<? extends AsmrNode<?>>>) node.body().instructions().iterator();
        while(iter.hasNext()) {
            AsmrAbstractInsnNode<?> insn = iter.next();
            Iterator<Insn> matchInsns = Stream.of(opcodes).iterator();
            while(matchInsns.hasNext()) {
                Insn matchInsn = matchInsns.next();
                if (!matchInsn.matches(insn)) break;
                if (!matchInsns.hasNext()) {
                    indices.add(insn);
                }
            }
        }
        return indices;
    }

    @SuppressWarnings({"unchecked"})
    public List<Pair<Integer, Integer>> findMatch(AsmrMethodNode node) {
        List<Pair<Integer, Integer>> indices = Lists.newArrayList();
        Iterator<AsmrAbstractInsnNode<? extends AsmrNode<?>>> iter = (Iterator<AsmrAbstractInsnNode<? extends AsmrNode<?>>>) node.body().instructions().iterator();
        while(iter.hasNext()) {
            AsmrAbstractInsnNode<?> insn = iter.next();
            Iterator<Insn> matchInsns = Stream.of(opcodes).iterator();
            while(matchInsns.hasNext()) {
                Insn matchInsn = matchInsns.next();
                if (!matchInsn.matches(insn)) break;
                if (!matchInsns.hasNext()) {
                    int index = indexOf(insn, node.body().instructions());
                    indices.add(Pair.create(index, index + opcodes.length));
                    // indices.add(indexOf(insn, node.body().instructions()) - (position == Position.AFTER ? 0 : opcodeList.size()));
                }
            }
        }
        return indices;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder().append("{");
        for(Insn insn : this.opcodes) {
            builder.append("[").append(insn.toString()).append("]").append(", ");
        }
        return builder.append("}").toString();
    }
}
