package biom4st3r.asmr_compiler.compilers.utils;

import biom4st3r.asmr_compiler.util.AsmrUtil;
import org.quiltmc.asmr.tree.AsmrNode;
import org.quiltmc.asmr.tree.insn.AsmrFieldInsnNode;
import org.quiltmc.asmr.tree.member.AsmrClassNode;
import org.quiltmc.asmr.tree.member.AsmrFieldNode;

public class FieldTarget extends MethodTarget {

    public FieldTarget(String... args) {
        super(args);
    }

    @Override
    public boolean matches(AsmrNode<?> obj) {
        if(obj instanceof AsmrFieldNode) {
            AsmrFieldNode node = (AsmrFieldNode) obj;
            boolean 
                matchname = node.name().value().equals(name) || this.skipName(),
                matchdesc = node.desc().value().equals(desc) || this.skipDesc(),
                matchowner = (node.parent() != null && AsmrUtil.parentOf(AsmrClassNode.class, node).name().value().equals(owner)) || this.skipOwner();
            return matchdesc && matchname && matchowner;
        } else if(obj instanceof AsmrFieldInsnNode) {
            AsmrFieldInsnNode node = (AsmrFieldInsnNode) obj;
            boolean 
                matchname = node.name().value().equals(name) || this.skipName(),
                matchdesc = node.desc().value().equals(desc) || this.skipDesc(),
                matchowner = node.owner().value().equals(owner) || this.skipOwner();
            return matchdesc && matchname && matchowner;
        }
        return false;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof FieldTarget) {
            FieldTarget target = (FieldTarget) obj;
            return this.name == target.name && this.desc == target.desc && this.owner == target.owner;
        }
        return false;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("[FieldTarget: ")
            .append("[owner]").append(this.skipOwner() ? "ANY" : owner)
            .append(" [name]").append(this.skipName() ? "ANY" : name)
            .append(" [desc]").append(this.skipDesc() ? "ANY" : desc)
            .append("]")
            .toString();
    }

}
