package biom4st3r.asmr_compiler.compilers.utils;

import java.util.List;
import biom4st3r.asmr_compiler.util.Lists;

public class Tree {
    public String[] info;
    public final Tree parent;
    public final List<Tree> children = Lists.newArrayList();
    public Tree(String info, Tree parent) {
        this.info = info.split("[\t ]");
        this.parent = parent;
    }
    public String getJoinedInfo() {
        return String.join(" ", info);
    }
    public void reassignInfo(String s) {
        this.info = s.split("[\t ]");
    } 
    public void addChild(Tree tree) {
        this.children.add(tree);
    }
}