package biom4st3r.asmr_compiler.compilers.utils;

import biom4st3r.asmr_compiler.Biom4st3rCompilerError;
import biom4st3r.asmr_compiler.ConverterDoDad;
import biom4st3r.asmr_compiler.util.AsmrUtil;
import biom4st3r.asmr_compiler.util.Stringhash;
import org.quiltmc.asmr.tree.AsmrNode;
import org.quiltmc.asmr.tree.insn.AsmrMethodInsnNode;
import org.quiltmc.asmr.tree.member.AsmrClassNode;
import org.quiltmc.asmr.tree.member.AsmrMethodNode;

public class MethodTarget {
    public final String desc;
    public final String name;
    public final String owner;

    public boolean hasSkippable() {
        return skipDesc() | skipName() | skipOwner();
    }

    public boolean ownerMatches(String s) {
        return this.skipOwner() || this.owner.equals(s);
    }

    public final int opcode;
    private static final String skip = "#SKIP";
    public MethodTarget(String... args) {
        int index = 0;
        if (args[0].equals("method")) {
            if (args.length < 4) throw new Biom4st3rCompilerError("Malformed Method instruction. Proper format 'method theMethodName (theDesc)V'");
            index = 1;
        }
        this.owner = args[index++];
        // owner_pred = (string)->string.equals(args[index++]);
        this.name = args[index++];
        // name_pred = (string)->string.equals(args[index++]);
        this.desc = args[index++];
        // desc_pred = (string)->string.equals(args[index++]);
        this.opcode = args.length == index ? -1 : ConverterDoDad.nameToOpcodeInt.getOrDefault(args[index], -1);

        // this.pattern = Pattern.compile((owner+name+desc)
        //     .replace("(", "\\(")
        //     .replace(")", "\\)")
        //     .replace("[", "\\[")
        //     .replace(skip, ".*")
        //     .replace(".*.*.*", ".*")
        //     .replace(".*.*", ".*")
        //     ,Pattern.DOTALL);
    }
    
    public boolean matches(AsmrNode<?> obj) {
        if(obj instanceof AsmrMethodNode) {
            AsmrMethodNode node = (AsmrMethodNode) obj;
            // String input = AsmrUtil.parentOf(AsmrClassNode.class, node).name().value() + node.name().value() + node.desc().value();
            // System.out.println(pattern.matcher(input).matches());
            boolean 
                matchname = node.name().value().equals(name) || this.skipName(),
                matchdesc = node.desc().value().equals(desc) || this.skipDesc(),
                matchowner = (node.parent() != null && AsmrUtil.parentOf(AsmrClassNode.class, node).name().value().equals(owner)) || this.skipOwner();
            // System.out.println(matchdesc && matchname && matchowner);
            return matchdesc && matchname && matchowner;
        } else if(obj instanceof AsmrMethodInsnNode) {
            AsmrMethodInsnNode node = (AsmrMethodInsnNode) obj;
            // String input = node.owner().value() + node.name().value() + node.desc().value();
            // System.out.println(pattern.matcher(input).matches());
            boolean 
                matchname = node.name().value().equals(name) || this.skipName(),
                matchdesc = node.desc().value().equals(desc) || this.skipDesc(),
                matchowner = node.owner().value().equals(owner) || this.skipOwner();
            System.out.println(matchdesc && matchname && matchowner);
            return matchdesc && matchname && matchowner;
        }
        throw new RuntimeException("Failed");
    }    

    protected boolean skipName() {
        return this.name.equals(skip);
    }
    protected boolean skipDesc() {
        return this.desc.equals(skip);
    }
    protected boolean skipOwner() {
        return this.owner.equals(skip);
    }
    public String[] internalGet() {
        return new String[]{owner,name,desc};
    }
    public boolean hasOpcode() {return opcode != -1;};

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof MethodTarget) {
            MethodTarget target = (MethodTarget) obj;
            return this.name == target.name && this.desc == target.desc && this.owner == target.owner;
        }
        return false;
    }
    @Override
    public int hashCode() {
        return Stringhash.hashCode(this.name+this.owner+this.desc);
    }
    @Override
    public String toString() {
        return new StringBuilder().append("[MethodTarget: ")
        .append("[owner]").append(this.skipOwner() ? "ANY" : owner)
        .append(" [name]").append(this.skipName() ? "ANY" : name)
        .append(" [desc]").append(this.skipDesc() ? "ANY" : desc)
        .append("]")
        .toString();
    }
}
