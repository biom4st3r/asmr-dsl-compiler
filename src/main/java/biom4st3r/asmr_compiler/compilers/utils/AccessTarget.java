package biom4st3r.asmr_compiler.compilers.utils;

import biom4st3r.asmr_compiler.Biom4st3rCompilerError;
import biom4st3r.asmr_compiler.compilers.AccessTransformerCompiler;

public class AccessTarget {

    public enum TargetType {
        CLASS,
        FIELD,
        METHOD
    }

    public final boolean add;
    public final String owner;
    public final String name;
    public final String desc;
    public final TargetType type;
    public final int modifier;

    public AccessTarget(String owner, String modString) { // Class Ctor
        if(modString.startsWith("-")) {
            add = false;
            this.modifier = AccessTransformerCompiler.map.getOrDefault(modString.substring(1).toUpperCase(), -1);
        } else if(modString.startsWith("+")) {
            add = true;
            this.modifier = AccessTransformerCompiler.map.getOrDefault(modString.substring(1).toUpperCase(), -1);
        } else throw new Biom4st3rCompilerError("Missing or Invalid prefix symbol on \nclass %s\n\t%s", owner, modString); // TODO
        if(modifier == -1) {
            throw new Biom4st3rCompilerError("Invalid modifier %s %s", modString, owner);
        }
        this.owner = owner;
        this.name = "";
        this.desc = name;
        this.type = TargetType.CLASS;
    }

    public AccessTarget(TargetType type, String owner, String name, String desc, String modString) {
        if(modString.startsWith("-")) {
            add = false;
            this.modifier = AccessTransformerCompiler.map.getOrDefault(modString.substring(1).toUpperCase(), -1);
        } else if(modString.startsWith("+")) {
            add = true;
            this.modifier = AccessTransformerCompiler.map.getOrDefault(modString.substring(1).toUpperCase(), -1);
        } else throw new Biom4st3rCompilerError("Missing or Invalid prefix symbol on \n%s %s.%s%s", type.name(), owner, name, desc); // TODO
        if(modifier == -1) {
            throw new Biom4st3rCompilerError("Invalid modifier %s %s.%s%s", modString, owner, name, desc);
        }
        this.owner = owner;
        this.name = name;
        this.desc = desc;
        this.type = type;
    }
}
