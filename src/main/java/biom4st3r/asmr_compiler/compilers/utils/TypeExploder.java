package biom4st3r.asmr_compiler.compilers.utils;

import java.util.Arrays;

import org.objectweb.asm.Type;

public class TypeExploder {

    private final Type[] args;
    public final Type returntype;
    public final int argLength;

    public Type[] getArgs() {
        return Arrays.copyOf(args, args.length); 
    }
    public TypeExploder(String desc) {
        this(Type.getArgumentTypes(desc), Type.getReturnType(desc));
    }
    public TypeExploder(Type[] args, Type returnType) {
        this.args = args;
        this.returntype = returnType;
        this.argLength = args.length;
    }
    public TypeExploder withReturnType(Type type) {
        return new TypeExploder(this.args, type);
    }
    public TypeExploder withArg(int index, Type type) {
        Type[] args = Arrays.copyOf(this.args, this.args.length);
        args[index] = type;
        return new TypeExploder(args, this.returntype);
    }
    public TypeExploder withArgs(Type[] type) {
        return new TypeExploder(type, this.returntype);
    }
    @Override
    public String toString() {
        return Type.getMethodDescriptor(this.returntype, this.args);
    }
    public String toDesc() {
        return this.toString();
    }
	public TypeExploder replaceMatching(String targetType0, String replacingType0) {
        Type targetType = Type.getObjectType(targetType0);
        Type replacingType = Type.getObjectType(replacingType0);
        TypeExploder desc = this;
        if (desc.returntype.equals(targetType)) {
            desc = desc.withReturnType(replacingType);
        }
        for (int i = 0; i < desc.args.length; i++) {
            if (desc.args[i].equals(targetType)) {
                desc = desc.withArg(i, replacingType);
            }
        }
        return desc;
	}
	public TypeExploder replaceMatching(Type targetType, Type replacingType) {
        TypeExploder desc = this;
        if (desc.returntype.equals(targetType)) {
            desc = desc.withReturnType(replacingType);
        }
        for (int i = 0; i < desc.args.length; i++) {
            if (desc.args[i].equals(targetType)) {
                desc = desc.withArg(i, replacingType);
            }
        }
        return desc;
	}
    public boolean argNeedsPop2(int i) {
        return this.args[i].equals(Type.LONG_TYPE) || this.args[i].equals(Type.DOUBLE_TYPE);
    }
    public boolean returnNeedsPop2() {
        return this.returntype.equals(Type.DOUBLE_TYPE) || this.returntype.equals(Type.LONG_TYPE);
    }
}