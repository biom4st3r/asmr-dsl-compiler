package biom4st3r.asmr_compiler.compilers.utils;

public class Pair<A,B> {

    public final A left;
    public final B right;

    public Pair(A left, B right) {
        this.left = left;
        this.right = right;
    }
    public Pair<A,B> withLeft(A newLeft) {
        return new Pair<A,B>(newLeft, this.right);
    }
    public Pair<A,B> withRight(B newRight) {
        return new Pair<A,B>(this.left, newRight);
    }
    public boolean isFull() {
        return left != null && right != null;
    }
	public static <A,B> Pair<A,B> create(A left, B right) {
		return new Pair<A,B>(left,right);
	}
}