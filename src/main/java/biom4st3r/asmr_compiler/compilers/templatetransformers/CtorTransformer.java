package biom4st3r.asmr_compiler.compilers.templatetransformers;

import biom4st3r.asmr_compiler.builder.Builder;
import biom4st3r.asmr_compiler.compilers.descriptions.CtorReplacementDesc;
import biom4st3r.asmr_compiler.insns_v2.MethodInsn;
import biom4st3r.asmr_compiler.insns_v2.TypeInsn;
import org.objectweb.asm.Opcodes;
import org.quiltmc.asmr.processor.AsmrProcessor;

public abstract class CtorTransformer extends TransformerGenerator {

    @Override
    public void read(AsmrProcessor processor) {
        Builder.withAllClasses(processor, this)
            .withAllMethods()
                .matchInsn(new TypeInsn(Opcodes.NEW, getDesc().targetClass))
                    .replaceType(getDesc().replacingClass)
                    .done()
                .matchInsn(new MethodInsn(Opcodes.INVOKESPECIAL, getDesc().targetClass, "<init>", "#SKIP"))
                    .replaceOwner(getDesc().replacingClass)
        ;
    }

    @Override
    public abstract CtorReplacementDesc getDesc();
    
}
