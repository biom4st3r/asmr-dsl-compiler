package biom4st3r.asmr_compiler.compilers.templatetransformers;

import biom4st3r.asmr_compiler.compilers.utils.TypeExploder;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Handle;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

public class TypeReplacementClassVisitor extends ClassVisitor {

    private final Type replacement;
    private final Type target;

    public TypeReplacementClassVisitor(Type target, Type replacement, ClassVisitor cv) {
        super(Opcodes.ASM8, cv);
        this.target = target;
        this.replacement = replacement;
    }

    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        super.visit(Opcodes.V1_8, access, name, signature, superName, interfaces);
    }
    @Override
    public MethodVisitor visitMethod(int access, String name, String descriptor, String signature,
            String[] exceptions) {                
        descriptor = new TypeExploder(descriptor).replaceMatching(target, replacement).toDesc();

        if(signature != null) {
            signature = signature.replace(target.getDescriptor(), replacement.getDescriptor());
        }
        return new TypeReplacementMethodVisitor(super.visitMethod(access, name, descriptor, signature, exceptions));

    }

    class TypeReplacementMethodVisitor extends MethodVisitor {

        public TypeReplacementMethodVisitor(MethodVisitor methodVisitor) {
            super(Opcodes.ASM8, methodVisitor);
        }

        @Override
        public void visitMethodInsn(int opcode, String owner, String name, String descriptor,
                boolean isInterface) {
            
            TypeExploder exploder = new TypeExploder(descriptor);
            exploder = exploder.replaceMatching(target, replacement);
            owner = owner.equals(target.getInternalName()) ? replacement.getInternalName() : owner;

            super.visitMethodInsn(opcode, owner, name, exploder.toDesc(), isInterface);
        }

        @Override
        public void visitInvokeDynamicInsn(String name, String descriptor, Handle bootstrapMethodHandle,
                Object... bootstrapMethodArguments) {
            TypeExploder te = new TypeExploder(descriptor).replaceMatching(target, replacement);
            for(int i = 0; i < bootstrapMethodArguments.length; i++) {
                if(bootstrapMethodArguments[i] instanceof Handle) {
                    Handle h = (Handle) bootstrapMethodArguments[i];
                    TypeExploder exploder = new TypeExploder(h.getDesc()).replaceMatching(target, replacement);
                    String owner = h.getOwner().equals(target.getInternalName()) ? replacement.getInternalName() : h.getOwner();
                    bootstrapMethodArguments[i] = new Handle(h.getTag(), owner, h.getName(), exploder.toDesc(), h.isInterface());
                } else if(bootstrapMethodArguments[i] instanceof Type) {
                    Type t = (Type) bootstrapMethodArguments[i];
                    if(t.equals(target)) {
                        bootstrapMethodArguments[i] = replacement;
                    }
                }
            }
            super.visitInvokeDynamicInsn(name, te.toDesc(), bootstrapMethodHandle, bootstrapMethodArguments);
        }

        @Override
        public void visitFrame(int type, int numLocal, Object[] local, int numStack, Object[] stack) {
            Object[] newLocal = new Object[local.length];
            for(int i = 0; i < local.length; i++) {
                if(target.getInternalName().equals(local[i])) {
                    newLocal[i] = replacement.getInternalName();
                } else newLocal[i] = local[i];
                
            }
            Object[] newStack = new Object[stack.length];
            for(int i = 0; i < stack.length; i++) {
                if(target.getInternalName().equals(stack[i])) {
                    newStack[i] = replacement.getInternalName();
                } else newStack[i] = stack[i];
            }
            super.visitFrame(type, numLocal, newLocal, numStack, newStack);
        }

        @Override
        public void visitLocalVariable(String name, String descriptor, String signature, Label start,
                Label end, int index) {
                    
            descriptor = descriptor.equals(target.getDescriptor()) ? replacement.getDescriptor() : descriptor;
            super.visitLocalVariable(name, descriptor, signature, start, end, index);
        }

        @Override
        public void visitTypeInsn(int opcode, String type) {
            super.visitTypeInsn(opcode, type.equals(target.getInternalName()) ? replacement.getInternalName() : type);
        }

        @Override
        public void visitFieldInsn(int opcode, String owner, String name, String descriptor) {
            owner = owner.equals(target.getInternalName()) ? replacement.getInternalName() : owner;
            super.visitFieldInsn(opcode, owner, name, descriptor);
        }
    }
}
