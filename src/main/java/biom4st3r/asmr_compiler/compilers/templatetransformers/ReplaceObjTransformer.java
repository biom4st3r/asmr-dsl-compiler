package biom4st3r.asmr_compiler.compilers.templatetransformers;

import java.util.Iterator;
import java.util.function.Predicate;
import java.util.stream.Stream;

import biom4st3r.asmr_compiler.builder.Builder;
import biom4st3r.asmr_compiler.compilers.descriptions.ObjectReplacementDesc;
import biom4st3r.asmr_compiler.compilers.utils.MethodTarget;
import biom4st3r.asmr_compiler.insns_v2.FieldInsn;
import biom4st3r.asmr_compiler.insns_v2.MethodInsn;
import biom4st3r.asmr_compiler.insns_v2.TypeInsn;
import org.objectweb.asm.Opcodes;
import org.quiltmc.asmr.processor.AsmrConstantPool;
import org.quiltmc.asmr.processor.AsmrProcessor;

public abstract class ReplaceObjTransformer extends TransformerGenerator {

    @Override
    public void read(AsmrProcessor processsor) {
        Predicate<AsmrConstantPool> matches = pool->false;
        for (Iterator<MethodTarget> iter = Stream.of(getDesc().targets).map(pair->pair.left).iterator(); iter.hasNext(); ) {
            MethodTarget target = iter.next();
            matches = matches.or(pool->pool.hasMethodRef(target.owner, target.name, target.desc));
        }
        
        Builder.withAllClasses(processsor, this)
            .withAllMethods()
            .mutateDesc0(s->s.replace("Ljava/util/Random;", "Lnet/minecraft/server/ShittyRandom;"))
            .matchInsn(new MethodInsn(Opcodes.INVOKEVIRTUAL, "java/util/Random", "#SKIP", "#SKIP"))
                .replaceOwner("net/minecraft/server/ShittyRandom")
                .done()
            .matchInsn(new FieldInsn(-1, "java/util/Random", "#SKIP", "#SKIP"))
                .replaceOwner("net/minecraft/server/ShittyRandom")
                .done()
            .matchInsn(new TypeInsn(-1, "java/util/Random"))
                .replaceType("net/minecraft/server/ShittyRandom")
                .done()
            .done()
        .done()
        ;
        // Builder.withAllClasses(processsor, this)
        //     .withMethod("#SKIP", "#SKIP", ".{0,}Ljava/util/Random;.*")
        //         .transformDesc(desc)
        //     ;

        // processsor.withClasses(name -> true, matches, acn -> {
        // // processsor.withAllClasses((acn)-> {
        //     acn.methods().stream().map((amn)-> amn.body().instructions()).forEach((insns)-> {
        //        for (int i = 0; i < insns.size(); i++) {
        //             AsmrAbstractInsnNode insn = insns.get(i);
        //             if (this.getDesc().isCanadate(insn)) {
        //                 if (insn instanceof AsmrMethodInsnNode) {
        //                     AsmrSliceCapture capture = processsor.refCapture(insns, before(i), after(i));
        //                     processsor.addWrite(this, capture, ()-> {
        //                         return getDesc().getReplacingInsn((AsmrMethodInsnNode) insn);
        //                     });
        //                 } else if (insn instanceof AsmrTypeInsnNode) {
        //                     AsmrNodeCapture capture = processsor.refCapture(((AsmrTypeInsnNode)insn).desc()); 
        //                     processsor.addWrite(this, capture, ()-> {
        //                         AsmrValueNode<String> node = new AsmrValueNode<>();
        //                         node.init(getDesc().replacingClass);
        //                         // TODO Should desc be renamed to operand or type or something 
        //                         return node;
        //                     });
        //                 } else if (insn instanceof AsmrFieldInsnNode) {
        //                     AsmrNodeCapture capture = processsor.refCapture(((AsmrFieldInsnNode)insn).desc());
        //                     processsor.addWrite(this, capture, ()-> {
        //                         AsmrValueNode node = new AsmrValueNode<>();
        //                         node.init(getDesc().getReplacingType().getDescriptor());
        //                         return node;
        //                     });
        //                 }
        //             }
        //         }
        //     });
        //     acn.methods().stream().map(amn->amn.body().localVariables()).forEach(lvt-> {
        //         lvt.forEach(lv-> {
        //             if (lv.desc().value().equals(getDesc().getTargetType().getDescriptor())) {
        //                 AsmrNodeCapture capture = processsor.refCapture(lv.desc());
        //                 processsor.addWrite(this, capture, ()-> {
        //                     AsmrValueNode<String> string = new AsmrValueNode<>();
        //                     string.init(getDesc().getReplacingType().getDescriptor());
        //                     return string;
        //                 });
        //             }
        //         });
        //     });
        //     acn.fields().stream().filter(afn->getDesc().getTargetType().getDescriptor().equals(afn.desc().value())).forEach(afn-> {
        //         AsmrNodeCapture capture = processsor.refCapture(afn.desc());
        //         processsor.addWrite(this, capture, ()->{
        //             AsmrValueNode node = new AsmrValueNode<>();
        //             node.init(getDesc().getReplacingType().getDescriptor());
        //             return node;
        //         });
        //     });
        //     for (AsmrMethodNode amn : acn.methods()) {
        //         final MethodDesc desc = new MethodDesc(amn.desc().value()).replaceMatching(getDesc().getTargetType(), getDesc().getReplacingType());
        //         // TODO should desc be seperated into Individual ValueNodes
        //         if (!amn.desc().value().equals(desc.toDesc())) {
        //             AsmrNodeCapture capture = processsor.refCapture(amn.desc());
        //             processsor.addWrite(this, capture, ()-> {
        //                 AsmrValueNode node = new AsmrValueNode<>();
        //                 node.init(desc.toDesc());
        //                 return node;
        //             });
        //         }
        //     }
        // });
    }
    
    @Override
    public abstract ObjectReplacementDesc getDesc();
}
