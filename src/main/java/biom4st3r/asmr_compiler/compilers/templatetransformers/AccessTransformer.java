package biom4st3r.asmr_compiler.compilers.templatetransformers;

import biom4st3r.asmr_compiler.builder.Builder;
import biom4st3r.asmr_compiler.builder.BuilderClass;
import biom4st3r.asmr_compiler.compilers.descriptions.AccessTransformerDesc;
import biom4st3r.asmr_compiler.compilers.utils.AccessTarget;
import org.quiltmc.asmr.processor.AsmrProcessor;

public abstract class AccessTransformer extends TransformerGenerator {
    @Override
    public void read(AsmrProcessor processor) {
        for(AccessTarget target : getDesc().getAll()) {
            BuilderClass clazz = Builder.withClass(processor, this, target.owner);
            switch(target.type.ordinal() + (target.add ? 0 : 3)) {
                case 0: clazz.addModifier(target.modifier).done();
                break;
                case 1: clazz.withField(target.owner, target.name, target.desc).addModifier(target.modifier).done().done();
                break;
                case 2: clazz.withMethod(target.owner, target.name, target.desc).addModifier(target.modifier).done().done();
                break;
                case 3: clazz.removeModifier(target.modifier).done();
                break;
                case 4: clazz.withField(target.owner, target.name, target.desc).removeModifier(target.modifier).done().done();
                break;
                case 5: clazz.withMethod(target.owner, target.name, target.desc).removeModifier(target.modifier).done().done();
                break;
                default:
                break;
            }
        }
        
        // for(AccessTarget target : this.getDesc().ALL) {
        //     UnifiedBuilder builder = new UnifiedBuilder().withClass(processor, this, target.owner);
        //     switch(target.type) {
        //         case CLASS:
        //             if(target.add) builder.class_addModifier(target.modifier);
        //             else builder.class_removeModifier(target.modifier);
        //             break;
        //         case FIELD:
        //             if(target.add) builder.withField(target.owner, target.name, target.desc).field_addModifier(target.modifier);
        //             else builder.withField(target.owner, target.name, target.desc).field_removeModifier(target.modifier);
        //             break;
        //         case METHOD:
        //             if(target.add) builder.withMethod(target.owner, target.name, target.desc).method_addModifier(target.modifier);
        //             else builder.withMethod(target.owner, target.name, target.desc).method_removeModifier(target.modifier);
        //             break;
        //     }
        //     builder.doneWithClass();
        // }

        // UnifiedBuilder builder = new UnifiedBuilder();
        // builder.withClass(processor, this, "net/minecraft/server/PlayerManager").withMethod("net/minecraft/server/PlayerManager", "protectedNonFinalMethod", "()V").method_addModifier(4096).doneWithClass();
        // builder.withClass(processor, this, "net/minecraft/server/PlayerManager").withField("net/minecraft/server/PlayerManager", "publicStaticField", "Ljava/lang/Object;").field_addModifier(4096).doneWithClass();
        // builder.withClass(processor, this, "net/minecraft/server/PlayerManager").withMethod("net/minecraft/server/PlayerManager", "packgePrivatefinalMethod", "()V").method_addModifier(4096).doneWithClass();
        // builder.withClass(processor, this, "net/minecraft/server/PlayerManager").class_addModifier(4096).doneWithClass();
        // builder.withClass(processor, this, "net/minecraft/server/TestEnum").withField("net/minecraft/server/TestEnum", "ENUM$VALUES", "[Lnet/minecraft/server/TestEnum;").field_removeModifier(4096).doneWithClass();
        // builder.withClass(processor, this, "net/minecraft/server/PlayerManager").withMethod("net/minecraft/server/PlayerManager", "packgePrivatefinalMethod", "()V").method_addModifier(2048).doneWithClass();
        // builder.withClass(processor, this, "net/minecraft/server/PlayerManager").withMethod("net/minecraft/server/PlayerManager", "packgePrivatefinalMethod", "()V").method_addModifier(128).doneWithClass();
        // builder.withClass(processor, this, "net/minecraft/server/PlayerManager").withMethod("net/minecraft/server/PlayerManager", "packgePrivatefinalMethod", "()V").method_addModifier(64).doneWithClass();
        // builder.withClass(processor, this, "net/minecraft/server/PlayerManager").withMethod("net/minecraft/server/PlayerManager", "packgePrivatefinalMethod", "()V").method_addModifier(32).doneWithClass();
        // builder.withClass(processor, this, "net/minecraft/server/TestEnum").withMethod("net/minecraft/server/TestEnum", "<init>", "(Ljava/lang/String;I)V").method_removeModifier(16).doneWithClass();
        // builder.withClass(processor, this, "net/minecraft/server/TestEnum").withField("net/minecraft/server/TestEnum", "ENUM$VALUES", "[Lnet/minecraft/server/TestEnum;").field_removeModifier(16).doneWithClass();
        // builder.withClass(processor, this, "net/minecraft/server/PlayerManager").withMethod("net/minecraft/server/PlayerManager", "protectedNonFinalMethod", "()V").method_removeModifier(16).doneWithClass();
        // builder.withClass(processor, this, "net/minecraft/server/PlayerManager").withField("net/minecraft/server/PlayerManager", "publicStaticField", "Ljava/lang/Object;").field_removeModifier(16).doneWithClass();
        // builder.withClass(processor, this, "net/minecraft/server/PlayerManager").class_addModifier(16).doneWithClass();
        // builder.withClass(processor, this, "net/minecraft/server/PlayerManager").withMethod("net/minecraft/server/PlayerManager", "packgePrivatefinalMethod", "()V").method_addModifier(16).doneWithClass();
        // builder.withClass(processor, this, "net/minecraft/server/PlayerManager").withMethod("net/minecraft/server/PlayerManager", "protectedNonFinalMethod", "()V").method_addModifier(4).doneWithClass();
        // builder.withClass(processor, this, "net/minecraft/server/TestEnum").withMethod("net/minecraft/server/TestEnum", "<init>", "(Ljava/lang/String;I)V").method_removeModifier(2).doneWithClass();
        // builder.withClass(processor, this, "net/minecraft/server/TestEnum").withField("net/minecraft/server/TestEnum", "ENUM$VALUES", "[Lnet/minecraft/server/TestEnum;").field_removeModifier(2).doneWithClass();
        // builder.withClass(processor, this, "net/minecraft/server/PlayerManager").withField("net/minecraft/server/PlayerManager", "publicStaticField", "Ljava/lang/Object;").field_removeModifier(2).doneWithClass();
        // builder.withClass(processor, this, "net/minecraft/server/TestEnum").withField("net/minecraft/server/TestEnum", "ENUM$VALUES", "[Lnet/minecraft/server/TestEnum;").field_addModifier(1).doneWithClass();
        // builder.withClass(processor, this, "net/minecraft/server/PlayerManager").class_removeModifier(1).doneWithClass();
        // builder.withClass(processor, this, "net/minecraft/server/PlayerManager").withField("net/minecraft/server/PlayerManager", "publicStaticField", "Ljava/lang/Object;").field_addModifier(1).doneWithClass();
        // builder.withClass(processor, this, "net/minecraft/server/PlayerManager").withMethod("net/minecraft/server/PlayerManager", "protectedNonFinalMethod", "()V").method_removeModifier(1).doneWithClass();
        // builder.withClass(processor, this, "net/minecraft/server/PlayerManager").withMethod("net/minecraft/server/PlayerManager", "packgePrivatefinalMethod", "()V").method_removeModifier(1).doneWithClass();
        // builder.withClass(processor, this, "net/minecraft/server/TestEnum").withMethod("net/minecraft/server/TestEnum", "<init>", "(Ljava/lang/String;I)V").method_addModifier(1).doneWithClass();
    }

    @Override
    public abstract AccessTransformerDesc getDesc();
}
