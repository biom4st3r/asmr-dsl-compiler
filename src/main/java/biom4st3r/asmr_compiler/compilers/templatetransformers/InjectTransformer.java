package biom4st3r.asmr_compiler.compilers.templatetransformers;

import biom4st3r.asmr_compiler.builder.Builder;
import biom4st3r.asmr_compiler.builder.Builder.Pos;
import biom4st3r.asmr_compiler.compilers.Compiler.Position;
import biom4st3r.asmr_compiler.compilers.descriptions.InjectTransformerDesc;
import biom4st3r.asmr_compiler.compilers.utils.MatchingInfo;
import biom4st3r.asmr_compiler.compilers.utils.MethodTarget;
import biom4st3r.asmr_compiler.insns_v2.Insn;
import org.quiltmc.asmr.processor.AsmrProcessor;

public abstract class InjectTransformer extends TransformerGenerator {
    @Override
    public void read(AsmrProcessor proc) {
        for(String clazz : getDesc().classTargets) {
            for(MethodTarget target : getDesc().matchingTargets(clazz)) {
                for(MatchingInfo info : getDesc().matchingInfo) {
                    Builder.withClass(proc, this, clazz)
                        .withMethod(target.owner, target.name, target.desc)
                        .matchInsn(info.getInsns().toArray(Insn[]::new))
                        .inject(info.ordinal, info.position == Position.BEFORE ? Pos.BEFORE : Pos.AFTER, getDesc().getBodyAddition());
                }
            }
        }
        // for (String clazz : getDesc().classTargets) {
        //     proc.withClass(clazz, (acn)-> {
        //         this.getDesc().getValidMethodStream(acn.methods()).forEach(amn -> {
        //             getDesc().matchingInfo.forEach(matchingInfo -> {
        //                 final List<Integer> insnTargetIndexes = matchingInfo.findMatch(amn);
        //                 for (int insnIndex : insnTargetIndexes) {
        //                     int index = matchingInfo.position == Position.BEFORE ? before(insnIndex) : after(insnIndex);
        //                     final AsmrSliceCapture cap = proc.refCapture(amn.body().instructions(), index, index);
        //                     proc.addWrite(this, (AsmrSliceCapture) cap, ()-> {
        //                         AsmrAbstractListNode list = new AsmrInstructionListNode<>();
        //                         List<Insn> insn = getDesc().getBodyAddition();
        //                         for (int i = 0; i < getDesc().getBodySize(); i++) {
        //                             insn.get(i).accept(amn.body());
        //                             AsmrAbstractInsnNode bodyInsn = (insn.get(i)).as(list);
        //                             list.addCopy(bodyInsn);
        //                         }
        //                         return list; 
        //                     });
        //                 }
        //                 if (getDesc().modifiesLineNumbers) {
        //                     for (AsmrAbstractInsnNode node : amn.body().instructions()) {
        //                         int[] prevLine = new int[]{-1};
        //                         AsmrIndex[] index = new AsmrIndex[]{null};
        //                         boolean foundInjectedLines = false;
        //                         if (node instanceof AsmrLineNumberNode) {
        //                             AsmrLineNumberNode line = (AsmrLineNumberNode)node;
        //                             if (line.line().value().intValue() != -1 && !foundInjectedLines) {
        //                                 prevLine[0] = line.line().value().intValue();
        //                                 if (index[0] == null) {
        //                                     index[0] = line.start().value();
        //                                 }
        //                             } else { // updates injected LineNumbers and replaces linenumbers after inject
        //                                 proc.addWrite(this, (AsmrNodeCapture)proc.refCapture(node), () -> {
        //                                     AsmrLineNumberNode lnn = new AsmrLineNumberNode(amn.body().instructions());
        //                                     lnn.line().init(++prevLine[0]);
        //                                     lnn.start().init(index[0]);
        //                                     return lnn;
        //                                 });
        //                             }
        //                         }
        //                     }
        //                 }
        //             });
        //         });
        //     });
        // }
    }

    @Override
    public abstract InjectTransformerDesc getDesc();
}