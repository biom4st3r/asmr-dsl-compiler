package biom4st3r.asmr_compiler.compilers.templatetransformers;

import static org.objectweb.asm.Opcodes.*;

import java.io.IOException;
import java.util.List;

import biom4st3r.asmr_compiler.OpcodeMethodVisitor;
import biom4st3r.asmr_compiler.OpcodeMethodVisitor.OpcodeClassVisitor;
import biom4st3r.asmr_compiler.compilers.descriptions.Description;
import biom4st3r.asmr_compiler.compilers.descriptions.Description.DescBundle;
import biom4st3r.asmr_compiler.util.Lists;
import biom4st3r.asmr_compiler.util.Stringhash;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Type;
import org.quiltmc.asmr.processor.AsmrProcessor;
import org.quiltmc.asmr.processor.AsmrTransformer;

public abstract class TransformerGenerator implements AsmrTransformer {
    
    public abstract Description getDesc();

    protected static int after(int i) {
        return i * 2 + 1;
    }
    protected static int before(int i) {
        return i * 2 + 2;
    }

    static ClassReader getReader(Type type) {
        try {
            return new ClassReader(type.getInternalName());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings({"rawtypes","unchecked"})
    public static DescBundle getTransformerClass(String className, Class superClass, Description desc, Class<? extends Description> descriptionClass, AsmrProcessor processor) {

        Type T_DESCRIPTION = Type.getType(descriptionClass);
        Type T_SUPER = Type.getType(superClass);
        // ClassReader SUPER_READER = getReader(T_SUPER);

        String fieldname = Stringhash.getHash(desc.getSourceFileName());
        className += fieldname;
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
        OpcodeClassVisitor cv = OpcodeMethodVisitor.newCv(cw);

        // SUPER_READER.accept(new ClassVisitor(ASM9,cv){
        //     @Override
        //     public MethodVisitor visitMethod(int access, String name, String descriptor, String signature,
        //             String[] exceptions) {
        //         if(!name.equals("read")) return null;
        //         return super.visitMethod(access, name, descriptor, signature, exceptions);
        //     }
        // }, ClassReader.EXPAND_FRAMES);

        cv.visit(V1_8, ACC_PUBLIC, className, null, T_SUPER.getInternalName(), null); {
            cv.visitField(ACC_PUBLIC|ACC_STATIC, fieldname, T_DESCRIPTION.getDescriptor(), null, null);
            
            OpcodeMethodVisitor mv = cv.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
            mv.visitCode(); {
                mv
                    .ALOAD(0)
                    .INVOKE_SPECIAL(T_SUPER.getInternalName(), "<init>", "()V")
                    .RETURN()
                    .visitMaxs(10, 10);
            } mv.visitEnd();

            mv = cv.visitMethod(ACC_PUBLIC, "getDesc", "()"+T_DESCRIPTION.getDescriptor(), null, null);
            mv.visitCode(); {
                mv
                    .GET_STATIC(className, fieldname, T_DESCRIPTION.getDescriptor())
                    .ARETURN()
                    .visitMaxs(10, 10);
            } mv.visitEnd();
        } cv.visitEnd();

        return new DescBundle(cw,className,fieldname,desc);
    }

    @Override
    public List<String> getPhases() {
        return Lists.newArrayList("READ_INITIAL");
    }

    @Override
    public void addDependencies(AsmrProcessor arg0) {
    }

}
