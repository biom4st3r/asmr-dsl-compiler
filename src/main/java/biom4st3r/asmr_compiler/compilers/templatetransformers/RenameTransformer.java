package biom4st3r.asmr_compiler.compilers.templatetransformers;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.Handle;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public class RenameTransformer extends ClassVisitor {
    public RenameTransformer(ClassVisitor classVisitor) {
        super(Opcodes.ASM8, classVisitor);
    }

    String class_name;
    String class_path;

    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        String[] s = name.split("/");
        this.class_name = s[s.length - 1];
        this.class_path = name;
        super.visit(version, access, name, signature, superName, interfaces);
    }

    public String mutateName(boolean condition, String name) {
        if (condition)
            return class_name + "$" + name;
        return name;
    }

    @Override
    public FieldVisitor visitField(int access, String name, String descriptor, String signature, Object value) {
        return super.visitField(access, mutateName(true, name), descriptor, signature, value);
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String descriptor, String signature,
            String[] exceptions) {
        MethodVisitor mv = super.visitMethod(access, mutateName(!name.equals("read"), name), descriptor, signature,
                exceptions);
        return new RenameMethodTransformer(mv);
    }
    class RenameMethodTransformer extends MethodVisitor {

        public RenameMethodTransformer(MethodVisitor methodVisitor) {
            super(Opcodes.ASM8, methodVisitor);
        }
        @Override
        public void visitMethodInsn(int opcode, String owner, String name, String descriptor, boolean isInterface) {
            super.visitMethodInsn(opcode, owner, mutateName(owner.equals(class_path), name), descriptor,
                    isInterface);
        }

        @Override
        public void visitFieldInsn(int opcode, String owner, String name, String descriptor) {
            super.visitFieldInsn(opcode, owner, mutateName(owner.equals(class_path), name), descriptor);
        }

        @Override
        public void visitInvokeDynamicInsn(String name, String descriptor, Handle bootstrapMethodHandle,
                Object... bootstrapMethodArguments) {
            if (bootstrapMethodHandle.getOwner().equals(class_path)) {
                bootstrapMethodHandle = new Handle(bootstrapMethodHandle.getTag(), bootstrapMethodHandle.getOwner(),
                        mutateName(true, bootstrapMethodHandle.getName()), bootstrapMethodHandle.getDesc(),
                        bootstrapMethodHandle.isInterface());
            }
            super.visitInvokeDynamicInsn(mutateName(true, name), descriptor, bootstrapMethodHandle,
                    bootstrapMethodArguments);
        }
    }
}
