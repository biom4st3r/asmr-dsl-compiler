package biom4st3r.asmr_compiler.compilers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.stream.Stream;

import biom4st3r.asmr_compiler.Biom4st3rCompilerError;
import biom4st3r.asmr_compiler.compilers.descriptions.Description;
import biom4st3r.asmr_compiler.compilers.utils.Tree;
import biom4st3r.asmr_compiler.util.Lists;
import biom4st3r.asmr_compiler.util.Maps;
import org.quiltmc.asmr.processor.AsmrProcessor;
import org.quiltmc.asmr.processor.AsmrTransformer;

public class Compiler {

    public static enum Transformers {
        v0_INJECT("v0 inject", InjectCompiler::builder),
        v0_REPLACE_OBJ("v0 objreplacer", ObjReplacerCompiler::builder),
        v0_REPLACE_CTOR("v0 ctorreplacer", CtorReplacerCompiler::builder),
        v0_ACCESS_TRANSFORMER("v0 accesstransformer", AccessTransformerCompiler::builder),
        ;
        private final String header;
        private final BiFunction<String,List<Tree>, Description> func;

        Transformers(String header, BiFunction<String,List<Tree>,Description> func) {
            this.header = header;
            this.func = func;
        }

        public BiFunction<String,List<Tree>, Description> getFunc() {
            if(this.func == null) {
                throw new Biom4st3rCompilerError("Biom4st3r forgot to add that Transformer builder Function to the enum...");
            }
            return func;
        }
        public boolean matches(Tree tree) {
            return tree.getJoinedInfo().toLowerCase().equals(header);
        }
        public static Transformers getType(Tree tree) {
            Optional<Transformers> o = Stream.of(values()).filter(t->t.matches(tree)).findFirst();
            if (o.isPresent()) return o.get();
            throw new Biom4st3rCompilerError("Unknown Header %s", tree.getJoinedInfo());
        }
    }

    public static Class<? extends AsmrTransformer>[] parseTransformer(AsmrProcessor processor, InputStream file, String fileName) {
        String[] insns = null;
        try {
            insns = getInsns(file, fileName);
        } catch (IOException e) {}
        return parseTransformer(processor, fileName, insns);
    }

    public static Class<? extends AsmrTransformer>[] parseTransformer(AsmrProcessor processor, String resourceName) {
        InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream(resourceName);
        return parseTransformer(processor, stream, resourceName);
    }

    public static Class<? extends AsmrTransformer>[] parseTransformer(AsmrProcessor processor, File file) {
        String[] insns = null;
        try {
            insns = getInsns(file);
        } catch (IOException e) {}
        return parseTransformer(processor, file.getName(), insns);
    }

    private static Class<? extends AsmrTransformer>[] parseTransformer(AsmrProcessor processor, String fileName, String[] insns) {
        List<Tree> trees = growTrees(insns);
        Description desc = Transformers.getType(trees.get(0)).getFunc().apply(fileName.replace(".asmr", ""), trees);
        // desc.setSourceFileName(fileName.replace(".asmr", ""));
        return desc.buildTransformerClass(processor);
    }

    public static String[] getInsns(File file) throws IOException {
        if (!file.isFile()) new Biom4st3rCompilerError("%s is not a file.", file.getName());
        if (!file.getName().endsWith(".asmr")) new Biom4st3rCompilerError("%s doesn't end in .asmr", file.getName());
        FileInputStream stream = new FileInputStream(file);
        return getInsns(stream, file.getName());
    }

    public static String[] getInsns(InputStream stream, String name) throws IOException {
        try {
            byte[] content = stream.readNBytes(Integer.MAX_VALUE);
            stream.close();
            return new String(content).split("\n");
        } catch(NullPointerException e) {
            throw new Biom4st3rCompilerError("%s contents could not be read or was empty.", name);
        }
    }

    public static List<Tree> growTrees(String[] string) {
        List<Tree> list = new ArrayList<>(string.length);
        int prevIndent = 0;
        Tree prevTree = null;
        for (String op : string) {
            int indent = indentionLevel(op);
            if(op.trim().equals("end")) break;
            if (prevIndent >= indent) {
                prevIndent = indent;
                if (prevTree == null || prevTree.parent == null || indent == 0) {
                    prevTree = new Tree(removeIndent(op), null);
                    list.add(prevTree);
                } else if (prevTree.parent != null) {
                     prevTree.parent.children.add(new Tree(removeIndent(op), prevTree.parent));
                }
            } else if (prevIndent < indent) {
                prevIndent = indent;
                Tree child = new Tree(removeIndent(op),prevTree);
                prevTree.addChild(child);
                prevTree = child;
            }
        }
        if (list.size() <= 0) throw new Biom4st3rCompilerError("Instruction Found, but tree returned empty");
        return list;
    }

    public static int indentionLevel(String s) {
        int i = 0;
        for (char c : s.toCharArray()) {
            if (c == ' ' || c == '\t') {
                i++;
            } else break;
        }
        return i;
    }
    
    private static String removeIndent(String s) {
        int i = 0;
        for (char c : s.toCharArray()) {
            if (c == ' ' || c == '\t') {
                i++;
            } else break;
        }
        return s.substring(i).replace("\r", "").replace("\n", "");
    }

    static void pruneTrees(List<Tree> trees, Predicate<Tree> tree) {
        for (int i = 0; i < trees.size(); /**nop */) {
            if (tree.test(trees.get(i))) {
                trees.remove(i);
            } else {
                i++;
            }
        }
    }

    static void enactMacros(List<Tree> trees) {
        Map<String,String> replacements = Maps.newHashMap();
        // Find Macros
        trees.stream().filter(Insns.DEFINE::matches).forEach(tree->replacements.put(tree.info[1], tree.info[2]));
        // Enact Macros
        List<Tree> allTrees = explodeTree(trees);
        
        allTrees
            .stream()
            .forEach(tree-> 
                replacements
                    .entrySet()
                    .stream()
                    .forEach(entry-> tree.reassignInfo(tree.getJoinedInfo().replace(entry.getKey(), entry.getValue()))));

        // for (int i = 0; i < allTrees.size(); i++) {
        //     for (Entry<String, String> entry : replacements.entrySet()) {
        //         String info = allTrees.get(i).getJoinedInfo();
        //         allTrees.get(i).reassignInfo(info.replace(entry.getKey(), entry.getValue()));
        //     }
        // }
        pruneTrees(trees, Insns.DEFINE::matches);
    }

    private static List<Tree> explodeTree(List<Tree> trees) {
        List<Tree> allTrees = Lists.newArrayList();
        trees.stream().peek(allTrees::add).forEach(tree->allTrees.addAll(explodeTree(tree.children)));
        // for (int i = 0; i < trees.size(); i++) {
        //     Tree tree = trees.get(i);
        //     allTrees.add(tree);
        //     allTrees.addAll(explodeTree(tree.children));
        // }
        return allTrees;
    }

    public enum Position {
        BEFORE,
        AFTER,
        ;
    }

    public static enum Insns {
        DEFINE("define"),
        CLASS("class"),
        METHOD("method"),
        MATCH("match"),
        ORDINAL("ordinal"), 
        BODY("body"),
        POSITION("position"),
        // obj replacer
        CLASS_TARGET("target",1),
        CLASS_REPLACER("replacer",1), 
        SWAP("swap"),
        // access tranformer
        FIELD("field")
        ;
        private String name;
        private int index;

        Insns(String name) {
            this.name = name;
            this.index = 0;
        }
        Insns(String name, int depth) {
            this(name);
            this.index = depth;
        }
        public boolean matches(Tree s) {
            return s.info[index].equals(this.name);
        }
    }

}
