package biom4st3r.asmr_compiler.compilers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import biom4st3r.asmr_compiler.Biom4st3rCompilerError;
import biom4st3r.asmr_compiler.compilers.descriptions.ObjectReplacementDesc;
import biom4st3r.asmr_compiler.compilers.utils.MethodTarget;
import biom4st3r.asmr_compiler.compilers.utils.Pair;
import biom4st3r.asmr_compiler.compilers.utils.Tree;

public class ObjReplacerCompiler extends Compiler {
    public static ObjectReplacementDesc builder(String sourceFile, List<Tree> trees) {
        if(!Transformers.v0_REPLACE_OBJ.matches(trees.get(0))) throw new Biom4st3rCompilerError("Transformer description is not valid for access transformer: %s", trees.get(0).getJoinedInfo());

        enactMacros(trees);
        Pair<String,String> pair = new Pair<String,String>(null, null);
        Iterator<Tree> iter = trees.stream().filter(Insns.CLASS::matches).iterator();
        while(iter.hasNext()) {
            Tree tree = iter.next();
            if (Insns.CLASS_TARGET.matches(tree)) {
                pair = pair.withLeft(tree.info[2]);
            } else if (Insns.CLASS_REPLACER.matches(tree)) {
                pair = pair.withRight(tree.info[2]);
            }
            if(pair.isFull()) break;
        }
        
        pruneTrees(trees, Insns.CLASS::matches);
        List<Pair<MethodTarget, MethodTarget>> list = new ArrayList<Pair<MethodTarget,MethodTarget>>();
        trees.stream().filter(Insns.SWAP::matches).forEach(tree-> {
            list.add(new Pair<MethodTarget,MethodTarget>(new  MethodTarget(tree.children.get(0).info), new MethodTarget(tree.children.get(1).info)));     
        });

        ObjectReplacementDesc desc = new ObjectReplacementDesc(sourceFile, pair.left, pair.right, list);
        return desc;
    }
}