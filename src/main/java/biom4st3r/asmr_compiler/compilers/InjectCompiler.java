package biom4st3r.asmr_compiler.compilers;

import java.util.List;
import java.util.stream.Collectors;

import biom4st3r.asmr_compiler.Biom4st3rCompilerError;
import biom4st3r.asmr_compiler.ConverterDoDad;
import biom4st3r.asmr_compiler.compilers.descriptions.InjectTransformerDesc;
import biom4st3r.asmr_compiler.compilers.utils.MatchingInfo;
import biom4st3r.asmr_compiler.compilers.utils.MethodTarget;
import biom4st3r.asmr_compiler.compilers.utils.Tree;
import biom4st3r.asmr_compiler.util.Lists;

public class InjectCompiler extends Compiler {
    public static InjectTransformerDesc builder(String sourcefile, List<Tree> trees) {
        if(!Transformers.v0_INJECT.matches(trees.get(0))) throw new Biom4st3rCompilerError("Transformer description is not valid for access transformer: %s", trees.get(0).getJoinedInfo());

        enactMacros(trees);
        InjectTransformerDesc desc = new InjectTransformerDesc(sourcefile);
        desc.classTargets.addAll(trees.stream().filter(Insns.CLASS::matches).map((tree)->tree.info[1]).collect(Collectors.toList()));
        pruneTrees(trees, Insns.CLASS::matches);
        try {
            desc.methodTargets.addAll(trees.stream().filter(Insns.METHOD::matches).map((tree)->new MethodTarget(tree.info)).collect(Collectors.toList()));
        } catch(java.lang.ArrayIndexOutOfBoundsException error) {
            throw new Biom4st3rCompilerError("Malformed Method instruction. Proper format 'method theMethodName (theDesc)V'");
        }
        pruneTrees(trees, Insns.METHOD::matches);
        desc.matchingInfo.addAll(buildMatchingInfo(trees));
        pruneTrees(trees, Insns.MATCH::matches);
        getBody(trees.stream().filter(Insns.BODY::matches).findFirst().get()).forEach((s)-> {
            desc.addInsn(s);
        });
        return desc;
    }

    private static List<String> getBody(Tree tree) {
        List<String> list = Lists.newArrayList();
        if (!tree.info[1].equals("bytecode")) throw new Biom4st3rCompilerError("%s is not a valid body type", tree.info[1]);
        for (Tree child : tree.children) {
            list.add(child.getJoinedInfo());
        }
        return list;
    }

    private static List<MatchingInfo> buildMatchingInfo(List<Tree> trees) {
        List<MatchingInfo> list = Lists.newArrayList();
        MatchingInfo info = new MatchingInfo();
        for (Tree tree : trees) {
            if (!Insns.MATCH.matches(tree)) continue;
            if ("start".equals(tree.info[1])) {    
                for (Tree child : tree.children) {
                    ConverterDoDad.nameToOpcodeInt.computeIfAbsent(child.info[0], string->{throw new Biom4st3rCompilerError("%s is not a valid java opcodes", child.getJoinedInfo());});
                    info.opcodeList.add(child.info);
                }
            } else {
                for (Tree child : tree.children) {
                    if (Insns.ORDINAL.matches(child)) {
                        String number = child.info[1];
                        info.ordinal = Integer.parseInt(number);
                    } else if (Insns.POSITION.matches(child)) {
                        String pos = child.info[1];
                        info.position = pos.toLowerCase().equals("before") ? Position.BEFORE : pos.toLowerCase().equals("after") ? Position.AFTER : null;
                        if (info.position == null) {
                            throw new Biom4st3rCompilerError("Invalid matching position child: %s", child.getJoinedInfo());
                        }
                    }
                }
                list.add(info);
                info = new MatchingInfo();
            }
        }
        return list;
    }
}