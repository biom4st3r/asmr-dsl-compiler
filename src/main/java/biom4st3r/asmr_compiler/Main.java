package biom4st3r.asmr_compiler;

import biom4st3r.asmr_compiler.compilers.Compiler;
import biom4st3r.asmr_compiler.compilers.utils.FieldTarget;
import biom4st3r.asmr_compiler.compilers.utils.MethodTarget;
import biom4st3r.asmr_compiler.util.AsmrUtil;
import com.google.common.base.Preconditions;

import static org.objectweb.asm.Opcodes.*;
import org.quiltmc.asmr.processor.AsmrProcessor;
import org.quiltmc.asmr.processor.AsmrTransformer;
import org.quiltmc.asmr.tree.AsmrValueListNode;
import org.quiltmc.asmr.tree.member.AsmrClassNode;

public class Main {
    public static final boolean doWrites = true;

    static void addTransformers(AsmrProcessor processor, Class<? extends AsmrTransformer>[] ts) {
        for(Class<? extends AsmrTransformer> i : ts) {
            System.out.println("Added " + i.getCanonicalName());
            processor.addTransformer(i);
        }
    }

    public static void main(String[] args) {
        AsmrProcessor processor = AsmrUtil.getProcessor();
        addTransformers(processor, Compiler.parseTransformer(processor, "access_transformer.asmr"));
        // addTransformers(processor, Compiler.parseTransformer(processor, "ctor_replacer.asmr"));
        // addTransformers(processor, Compiler.parseTransformer(processor, "objectReplacer.asmr"));
        // addTransformers(processor, Compiler.parseTransformer(processor, "injectTransformer.asmr"));


        try {
            processor.process();
            System.gc();
        } catch(Throwable t) { t.printStackTrace(); }
        
        // AsmrUtil.toFile("net/minecraft/server/PlayerManager", "PlayerManager" + System.nanoTime(), processor);
        // AsmrUtil.toFile("net/minecraft/server/TestEnum", "TestEnum" + System.nanoTime(), processor);
        AsmrUtil.toFile("net/minecraft/server/AccessTest", "AccessTest", processor);
        

        try {
            testTransomfation(processor);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private static boolean contains(AsmrValueListNode<Integer> list, int i) {
        return list.stream().mapToInt(m->m.value().intValue()).filter(v->v==i).findFirst().isPresent();
    }

    private static void testTransomfation(AsmrProcessor processor) throws Throwable {
        String className = "net/minecraft/server/AccessTest";
        AsmrClassNode clazz = processor.findClassImmediately("net/minecraft/server/AccessTest");
        contains(clazz.modifiers(), ACC_PUBLIC);
        contains(clazz.modifiers(), ACC_FINAL);
        {
            MethodTarget target = new MethodTarget(className,"publicFinalSyntheticMethod","()Ljava/lang/Void;");
            AsmrValueListNode<Integer> list = clazz.methods().stream().filter(target::matches).findFirst().get().modifiers();

            Preconditions.checkArgument(contains(list, ACC_PUBLIC));
            Preconditions.checkArgument(contains(list, ACC_FINAL));
            Preconditions.checkArgument(contains(list, ACC_SYNTHETIC));
        }
        {
            FieldTarget target = new FieldTarget(className,"publicFinalSyntheticField","Ljava/lang/Void;");
            AsmrValueListNode<Integer> list = clazz.fields().stream().filter(target::matches).findFirst().get().modifiers();
            Preconditions.checkArgument(contains(list, ACC_PUBLIC));
            Preconditions.checkArgument(contains(list, ACC_FINAL));
            Preconditions.checkArgument(contains(list, ACC_SYNTHETIC));
        }
        {
            MethodTarget target = new MethodTarget(className,"cleanMethod","()V");
            AsmrValueListNode<Integer> list = clazz.methods().stream().filter(target::matches).findFirst().get().modifiers();
            Preconditions.checkArgument(list.size() == 0);
        }
        {
            MethodTarget target = new MethodTarget(className,"publicSyncMethod","()V");
            AsmrValueListNode<Integer> list = clazz.methods().stream().filter(target::matches).findFirst().get().modifiers();
            Preconditions.checkArgument(contains(list, ACC_PUBLIC));
            Preconditions.checkArgument(contains(list, ACC_SYNCHRONIZED));
            Preconditions.checkArgument(!contains(list, ACC_PROTECTED));
            Preconditions.checkArgument(!contains(list, ACC_FINAL));
        }
        {
            MethodTarget target = new MethodTarget(className,"protectedStaticMethod","()[Ljava/lang/Object;");
            AsmrValueListNode<Integer> list = clazz.methods().stream().filter(target::matches).findFirst().get().modifiers();
            Preconditions.checkArgument(!contains(list, ACC_PUBLIC));
            Preconditions.checkArgument(!contains(list, ACC_FINAL));
            Preconditions.checkArgument(contains(list, ACC_STATIC));
            Preconditions.checkArgument(contains(list, ACC_PROTECTED));
        }
        {
            FieldTarget target = new FieldTarget(className,"protectedStaticField","[Ljava/lang/Object;");
            AsmrValueListNode<Integer> list = clazz.fields().stream().filter(target::matches).findFirst().get().modifiers();
            Preconditions.checkArgument(!contains(list, ACC_PUBLIC));
            Preconditions.checkArgument(!contains(list, ACC_FINAL));
            Preconditions.checkArgument(contains(list, ACC_STATIC));
            Preconditions.checkArgument(contains(list, ACC_PROTECTED));
        }
        {
            FieldTarget target = new FieldTarget(className,"publicField","Ljava/lang/Void;");
            AsmrValueListNode<Integer> list = clazz.fields().stream().filter(target::matches).findFirst().get().modifiers();
            Preconditions.checkArgument(contains(list, ACC_PUBLIC));
            Preconditions.checkArgument(!contains(list, ACC_FINAL));
            Preconditions.checkArgument(!contains(list, ACC_PROTECTED));
        }
        {
            FieldTarget target = new FieldTarget(className,"cleanField","Ljava/lang/Void;");
            AsmrValueListNode<Integer> list = clazz.fields().stream().filter(target::matches).findFirst().get().modifiers();
            Preconditions.checkArgument(list.size() == 0);
        }
    }

}
